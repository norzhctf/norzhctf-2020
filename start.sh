#!/bin/bash

echo "Starting Indus containers"
pushd indus/alarm/docker; ./start.sh; popd

echo "Starting Reverse containers"
pushd reverse/door_lock/docker; ./start.sh; popd

echo "Starting Web containers"
pushd web/intranet/docker; ./start.sh; popd
pushd web/ip_camera/docker; ./start.sh; popd
pushd web/passgen/docker; ./start.sh; popd
pushd web/system_alive_checker/docker; ./start.sh; popd

echo "All containers should be up (please check!)"
screen -ls

echo "Running autoheal"
pushd infra/scripts/; screen -dmS heal python autoheal.py; popd
