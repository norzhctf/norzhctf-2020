.ONESHELL:
.DEFAULT_GOAL := all

SHELL := /bin/bash
BOLD := $(shell tput bold)
RED := $(shell tput setaf 1)
GREEN := $(shell tput setaf 2)
YELLOW := $(shell tput setaf 3)
RESET := $(shell tput sgr0)

.PHONY: all
all: prep plan apply cleanup

.PHONY: clean
clean: prep plan-destroy destroy cleanup

.PHONY: build
build:
	@echo "$(GREEN)$(BOLD)== Building Docker container ==$(RESET)"
	@cd ../docker/
	@docker-compose pull
	@docker-compose build
	@docker-compose push
	@cd -

.PHONY: prep
prep: ssh.mk
	@echo "$(GREEN)$(BOLD)== Configuring Terraform ==$(RESET)"
	@$(eval include ssh.mk)
	@ssh-add .ssh/id_ed25519
	if [[ $${?} != 0 ]]; then rm -f ssh.mk; echo -e "$(RED)$(BOLD)Could not open a connection to the SSH agent, try again!$(RESET)"; $(MAKE) cleanup; exit 1; fi
	@terraform init \
		-lock=true \
		-input=false \
		-upgrade=true \
		-plugin-dir="$(HOME)/.terraform.d/plugins/linux_amd64" \
		-force-copy \
		-get=true \
		-get-plugins=true \
		-verify-plugins=true

.PHONY: ssh-agent
ssh-agent: ssh.mk
ssh.mk:
	@echo "$(GREEN)$(BOLD)== Creating SSH agent ==$(RESET)"
	@$(shell ssh-agent -s | sed 's/"//g; s/=/:=/g; s/\; /\n/g' | grep -vE "(export)|(echo)" | sed 's/^\(.*\)/export \1/' >$@)

.PHONY: plan
plan: prep
	@echo "$(GREEN)$(BOLD)== Planning for building ==$(RESET)"
	@$(eval include ssh.mk)
	@terraform plan \
		-lock=true \
		-input=false \
		-refresh=true

.PHONY: plan-destroy
plan-destroy: prep
	@echo "$(GREEN)$(BOLD)== Planning for destroying ==$(RESET)"
	@$(eval include ssh.mk)
	@terraform plan \
		-lock=true \
		-input=false \
		-refresh=true \
		-destroy

.PHONY: apply
apply: prep
	@echo "$(GREEN)$(BOLD)== Building Terraform managed infrastructure ==$(RESET)"
	@$(eval include ssh.mk)
	@terraform apply \
		-lock=true \
		-input=false \
		-refresh=true \
		-auto-approve \
		-parallelism=10

.PHONY: destroy
destroy: prep
	@echo "$(GREEN)$(BOLD)== Destroying Terraform managed infrastructure ==$(RESET)"
	@terraform destroy \
		-lock=true \
		-input=false \
		-refresh=true \
		-auto-approve \
		-parallelism=10

cleanup:
	@echo "$(GREEN)$(BOLD)== Cleanup ==$(RESET)"
	@$(eval -include ssh.mk)
	@ssh-add -D
	@ssh-agent -k >/dev/null 2>&1
	@rm -f ssh.mk