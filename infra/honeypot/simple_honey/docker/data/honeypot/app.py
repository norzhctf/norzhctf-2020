from pprint import pprint

import requests
import urllib
import json
import socket

from elasticsearch import Elasticsearch
from elasticsearch import helpers
from elasticsearch.serializer import JSONSerializer

es = Elasticsearch(['http://honeypot:kf9G3V82rInx@elasticsearch-kn5l9h.norzh.nuclea:80'])

PORT = 80

def add_data(src_ip,port,ip):
    src_ip = src_ip[0]
    obj = {
        "player_ip" : src_ip,
        "honeypot_port" : port,
        "remote_ip" : ip.decode()
    }
    pprint(obj)
    es.index(index='honeypot', doc_type='doc', body=obj)

def get_my_ip():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('elasticsearch-kn5l9h.norzh.nuclea', 5454))
    ip = sock.recv(4096)
    sock.close()
    return ip

IP = get_my_ip()

if __name__ == "__main__" :
    print(f"IP : {IP}")
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        sock.bind(('', PORT ))
        sock.listen(5)

        while True:
            client, address = sock.accept()
            add_data(address,PORT,IP) 
            client.close()  
        sock.close() 
    except KeyboardInterrupt:
        sock.close()
