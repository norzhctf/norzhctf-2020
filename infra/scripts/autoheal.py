#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Author: Baptiste MOINE <contact@bmoine.fr>.
import time
import docker
import logging
import traceback

HEALTHY_STATUS = 'healthy'
STARTING_STATUS = 'starting'
INTERVAL = 5

## Logging configuration.
logger = logging.getLogger(__name__)    
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
logger.addHandler(handler)
logger.setLevel(logging.INFO)

## Docker client.
logger.info('Connecting to the Docker daemon...')
client = docker.from_env()

while True:
    try:
        logger.debug('Listing containers...')
        containers = client.containers.list()

        if containers:
            logger.debug('Docker containers:')
            for container in containers:
                state = client.api.inspect_container(container.id)
                if 'Health' in state['State']:
                    healthcheck = state['State']['Health']['Status']
                    logger.debug('%s (based on %s): %s' % (container.name, container.image.tags[0], healthcheck))
                    if healthcheck != HEALTHY_STATUS and healthcheck != STARTING_STATUS:
                        logger.info('Restarting %s...' % (container.name))
                        container.restart()
                else:
                    logger.debug('Skiping %s (no healthcheck)...' % container.name)
        else:
            logger.error('No containers to check...')

        logger.debug('Sleeping %d seconds...' % (INTERVAL))
        time.sleep(INTERVAL)
    except (ValueError, TypeError, OSError, IOError, IndexError):
        logger.error(traceback.format_exc())