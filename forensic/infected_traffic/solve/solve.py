from androguard.core.analysis.analysis import ExternalClass
from androguard.core.bytecodes import apk, dvm
from androguard.misc import AnalyzeAPK
from Crypto.Cipher import ARC4
import base64
from binascii import unhexlify

import os
import sys
import re


def extract_apk(capture_path):
    extract_cmd = f"""
    tshark -o adb.tcp.port:5555 -r {capture_path} -Y "adb_service.data" -Tfields -e adb_service.data | tr -d '\n:' | sed 's/44415441........//g' | xxd -r -p > tmp.apk
    """
    os.system(extract_cmd)
    return "tmp.apk"

def find_key(path):
    a, d, dx = AnalyzeAPK(path)
    classes = list(dx.find_classes('Lwocwvy/czyxoxmbauu/slsa/c;'))[0]
    src = classes.get_vm_class().get_source()
    match = re.search(r".*?this\.h = \"(.*?)\"",src)
    print("Found key : " +  match.group(1))
    return match.group(1).encode()

def decode_flow(path, key):
    extract_data = f"""
    tshark -r {path} -Tfields -e http.file_data > tmp.txt
    """
    os.system(extract_data)
    f = open('tmp.txt','rb').read()
    for i in f.split(b'\n'):
        if(i.startswith(b'p=')):
            if(i[2:]!=b"1" and i[2:]!=b""):
                a = unhexlify(base64.b64decode(i[2:].replace(b'\\n',b'')))
                cipher = ARC4.new(key)
                clear = cipher.decrypt(a)
                if(b'ENSIBS' in clear):
                    print(clear)
                print()
        if(i.startswith(b'<tag>')):
            i = i.replace(b'<tag>',b'').replace(b'</tag>',b'')
            if(i != b'**2**0**0**' and i!=b'2453512'):
                a = unhexlify(base64.b64decode(i.replace(b'\\n',b'')))
                cipher = ARC4.new(key)
                clear = cipher.decrypt(a)
                if(b'ENSIBS' in clear):
                    print(clear)

if __name__ == '__main__':
    if(len(sys.argv) < 2):
        print(f'Usage : {sys.argv[0]} <network capture>')
        sys.exit(-1)
    path = extract_apk(sys.argv[1])
    key = find_key(path)
    decode_flow(sys.argv[1],key)