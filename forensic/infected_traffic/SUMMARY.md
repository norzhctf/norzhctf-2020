# Nom du challenge

Infected traffic

## Description du challenge

Un des téléphones du parc d'entreprise de Norzh Nuclea a été infecté.
Cependant grâce à un SIEM 2.0 ( 'tcpdump' ) nous avons pu récupérer une capture réseau. Décodez les données extraites par le malware afin de qualifier l'ampleur de la fuite de données.

/!\ Pour réussir ce challenge, vous manipulerez un vrai malware. Le malware a été désamorcé, mais cependant ne l'éxecutez pas sur un vrai téléphone /!\

Fichiers : (../files/anubis.pcap)

## Auteur

[Areizen](https://www.twitter.com/Areizen_)