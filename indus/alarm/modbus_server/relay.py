#!/usr/bin/env python3
import time
import gpiozero

relay = gpiozero.OutputDevice(18, active_high=False, initial_value=False)

def set_relay(status):
    if status:
        print("Setting relay: ON")
        relay.on()
    else:
        print("Setting relay: OFF")
        relay.off()

def main():
    set_relay(False)

    while True:
        set_relay(True)
        time.sleep(10)
        set_relay(False)
        time.sleep(10)

if __name__ == '__main__':
    main()
