from pymodbus.client.sync import ModbusTcpClient as ModbusClient

from pymodbus.mei_message import ReadDeviceInformationRequest

import logging
import time

FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

COUNT = 1  # number of bits/register to read
SLAVE = 0x00  # slave id to read from
REGISTER = 1  # register index
ADDRESS = 0  # address to read on
OFF_VALUE = 0  # relay off value
ON_VALUE = 1  # relay on value

def run_sync_client():
    with ModbusClient('192.168.14.2', port=5020) as client:
        rq = ReadDeviceInformationRequest(unit=SLAVE)
        rr = client.execute(rq)
        info = ' - '.join(list(map(lambda x: x.decode(), rr.information.values())))
        log.info(f'Server info: {info}')

        log.info(f'Switching on the relay...')
        rq = client.write_register(address=ADDRESS, value=ON_VALUE, unit=SLAVE)
        rr = client.read_holding_registers(address=ADDRESS, count=COUNT, unit=SLAVE)
        if rr.registers == [ON_VALUE]:
            log.info('Successfully switched on the relay!')
        else:
            log.error('Failed to switch on the relay...')

if __name__ == "__main__":
    run_sync_client()
