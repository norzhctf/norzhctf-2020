#!/usr/bin/env python3

from pymodbus.server.asynchronous import StartTcpServer
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from twisted.internet.task import LoopingCall

import gpiozero
import logging
import time

FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

COUNT = 1  # number of bits/register to read
SLAVE = 0x00  # slave id to read from
REGISTER = 1  # register index
ADDRESS = 0  # address to read on
OFF_VALUE = 0  # relay off value
ON_VALUE = 1  # relay on value
DELAY = 10  # delay before reseting the register

relay = gpiozero.OutputDevice(18, active_high=False, initial_value=False)

def set_relay(status):
    if status:
        log.info('Setting relay: ON')
        relay.on()
    else:
        log.info('Setting relay: OFF')
        relay.off()

def register_check(context):
    """Check if the register value has been changed."""
    values = context[SLAVE].getValues(REGISTER, ADDRESS, count=COUNT)
    log.debug(f'Register value: {values}')

    if values == [ON_VALUE]:
        set_relay(True)

        time.sleep(DELAY)

        set_relay(False)
        context[SLAVE].setValues(REGISTER, ADDRESS, [OFF_VALUE])

def run_server():
    """Initialize the modbus server."""
    # ----------------------------------------------------------------------- #
    # data store
    # ----------------------------------------------------------------------- #    
    block = ModbusSequentialDataBlock.create()
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block)
    context = ModbusServerContext(slaves=store, single=True)

    # ----------------------------------------------------------------------- #
    # server information
    # ----------------------------------------------------------------------- #
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'Norzh Nuclea'
    identity.ProductCode = 'Nuclalarm'
    identity.VendorUrl = 'http://git.norzh.nuclea/engineering/alarm/'
    identity.ProductName = 'Alarm'
    identity.ModelName = 'Nuclear Power Plant Alarm'
    identity.MajorMinorRevision = '1.0.0'

    # ----------------------------------------------------------------------- #
    # modbus server
    # ----------------------------------------------------------------------- #
    loop = LoopingCall(f=register_check, context=context)
    loop.start(1, now=False) # loop every seconds
    StartTcpServer(context, identity=identity, address=('0.0.0.0', 5020))

if __name__ == '__main__':
    run_server()
