#
# Flask container
#
# Written by:
#   Baptiste MOINE <contact@bmoine.fr>
#

# Builder.
FROM python:3-slim AS builder

RUN apt update && \
    apt install -y --no-install-recommends --no-install-suggests build-essential gcc

RUN python3 -m venv /opt/venv

ENV PATH="/opt/venv/bin:${PATH}"

COPY requirements.txt .
RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install -r requirements.txt

FROM python:3-alpine AS runner
COPY --from=builder /opt/venv /opt/venv

RUN apk add netcat-openbsd build-base curl libcurl curl-dev

ENV PATH="/opt/venv/bin:${PATH}"

RUN python3 -m pip install pycurl pymodbus

WORKDIR /data/