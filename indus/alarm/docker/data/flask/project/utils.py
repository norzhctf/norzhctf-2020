#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import pycurl
from io import BytesIO
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from .config import (ALARM_MODBUS_COUNT, ALARM_MODBUS_SLAVE, ALARM_MODBUS_REGISTER, ALARM_MODBUS_ADDRESS, ALARM_MODBUS_OFF_VALUE, ALARM_MODBUS_ON_VALUE, ALARM_MODBUS_PINCODE, ALARM_MODBUS_HOST, ALARM_MODBUS_PORT)

def do_update(package_uri):
    """fetch firmware package and update it."""
    try:
        buffer = BytesIO()
        c = pycurl.Curl()
        c.setopt(c.URL, package_uri)
        c.setopt(c.WRITEDATA, buffer)
        c.perform()
        return_code = c.getinfo(pycurl.HTTP_CODE)
        c.close()
        # TODO: write the firmware on the disk
        if return_code and return_code != 200:
            result = False
        else:
            result = True
    except:
        result = False
    finally:
        return result

def check_alarm_state():
    """get alarm state using modbus."""
    try:
        with ModbusClient(ALARM_MODBUS_HOST, port=ALARM_MODBUS_PORT) as client:
            rr = client.read_holding_registers(address=ALARM_MODBUS_ADDRESS, count=ALARM_MODBUS_COUNT, unit=ALARM_MODBUS_SLAVE)
            if rr.registers == [ALARM_MODBUS_ON_VALUE]:
                result = True
            else:
                result = False

            return result
    except:
        result = False
    finally:
        return result

def update_alarm_state(pin, state):
    """update alarm state using modbus."""
    try:
        if pin == ALARM_MODBUS_PINCODE:
            with ModbusClient(ALARM_MODBUS_HOST, port=ALARM_MODBUS_PORT) as client:
                if state == 'activate':
                    value = ALARM_MODBUS_ON_VALUE
                else:
                    value = ALARM_MODBUS_OFF_VALUE

                client.write_register(address=ALARM_MODBUS_ADDRESS, value=value, unit=ALARM_MODBUS_SLAVE)
                result = True
        else:
            result = False
    except:
        result = False
    finally:
        return result
