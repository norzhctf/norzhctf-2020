#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, jsonify, request, make_response)
from .utils import (do_update, check_alarm_state, update_alarm_state)

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
def index():
    response = render_template('index.html'), 200
    return response

@main.route('/update', methods=['GET', 'POST'])
def update():
    package_uri = 'http://example.com/update.zip' if request.method == 'GET' else request.form.get('uri')

    result = do_update(package_uri)
    if result == False:
        response = make_response('An error occured while trying to load the firmware package, please consider specifying a new one!', 500)
    else:
        response = make_response('The firmware has been updated successfully! A reboot is needed for the update to be effective.', 200)

    response.mimetype = 'text/plain'

    return response

@main.route('/check-version', methods=['GET'])
def check_version():
    response = make_response('0.2', 200)

    response.mimetype = 'text/plain'

    return response

@main.route('/alarm-state', methods=['GET'])
def alarm_state():
    if check_alarm_state():
        response = make_response('ON', 200)
    else:
        response = make_response('OFF', 200)

    response.mimetype = 'text/plain'

    return response

@main.route('/alarm-state', methods=['POST'])
def change_alarm_state():
    pin = request.form.get('pin')
    state = request.form.get('state')
    if update_alarm_state(pin, state):
        response = make_response('Alarm state changed successfully', 200)
    else:
        response = make_response('An error occured while trying to change alarm state', 500)

    response.mimetype = 'text/plain'

    return response
