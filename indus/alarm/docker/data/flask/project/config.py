#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import environ

# environment variables.
APP_HOST = environ.get('APP_HOST', '127.0.0.1')
APP_PORT = int(environ.get('APP_PORT', 8080))
APP_SECRET_KEY = environ.get('APP_SECRET_KEY', '12345')

# modbus
ALARM_MODBUS_PINCODE = environ.get('ALARM_MODBUS_PINCODE', '6498')  # PIN code used to change alarm status
ALARM_MODBUS_COUNT = int(environ.get('ALARM_MODBUS_COUNT', 1))  # number of bits/register to read
ALARM_MODBUS_SLAVE = int(environ.get('ALARM_MODBUS_SLAVE', 0x00))  # slave id to read from
ALARM_MODBUS_REGISTER = int(environ.get('ALARM_MODBUS_REGISTER', 1))  # register index
ALARM_MODBUS_ADDRESS = int(environ.get('ALARM_MODBUS_ADDRESS', 0))  # address to read on
ALARM_MODBUS_OFF_VALUE = int(environ.get('ALARM_MODBUS_OFF_VALUE', 0))  # relay off value
ALARM_MODBUS_ON_VALUE = int(environ.get('ALARM_MODBUS_ON_VALUE', 1))  # relay on value
ALARM_MODBUS_HOST = environ.get('ALARM_MODBUS_HOST', '127.0.0.1')
ALARM_MODBUS_PORT = int(environ.get('ALARM_MODBUS_PORT', 5020))