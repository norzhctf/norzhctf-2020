server {
    listen                                          80 default;
    listen                                          [::]:80 default;
    server_name                                     plc-6486b4e.reactor.norzh.nuclea plc-6486b4e;
    more_set_headers                                "Server: plc-6486b4e.reactor.norzh.nuclea";

    access_log                                      /usr/share/nginx/log/plc-6486b4e.access.log main;
    error_log                                       /usr/share/nginx/log/plc-6486b4e.error.log warn;

    charset                                         utf-8;
    keepalive_timeout                               70;

    client_max_body_size                            200M;

    location / {
        proxy_read_timeout                          300;
        proxy_connect_timeout                       300;
        proxy_redirect                              off;

        proxy_set_header                            Host $host;
        proxy_set_header                            X-Real-IP $remote_addr;
        proxy_set_header                            X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header                            X-Forwarded-Proto $scheme;
        proxy_pass                                  http://127.0.0.1:8000/;
    }
}

server {
    listen                                          443 ssl http2;
    listen                                          [::]:443 ssl http2;
    server_name                                     plc-6486b4e.reactor.norzh.nuclea plc-6486b4e;

    more_set_headers                                "Server: plc-6486b4e.reactor.norzh.nuclea";

    access_log                                      /usr/share/nginx/log/plc-6486b4e.access.log main;
    error_log                                       /usr/share/nginx/log/plc-6486b4e.error.log warn;

    charset                                         utf-8;
    keepalive_timeout                               70;

    client_max_body_size                            200M;

    location / {
        proxy_read_timeout                          300;
        proxy_connect_timeout                       300;
        proxy_redirect                              off;

        proxy_set_header                            Host $host;
        proxy_set_header                            X-Real-IP $remote_addr;
        proxy_set_header                            X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header                            X-Forwarded-Proto $scheme;
        proxy_pass                                  http://127.0.0.1:8000/;
    }

    # ssl                                             on;                                                 # It is recommended to use the ssl parameter of the listen directive instead of this directive.

    ssl_protocols                                   TLSv1.2;

    ssl_certificate                                 /etc/ssl/private/plc-6486b4e.reactor.norzh.nuclea.pem;                 # RSA certificate in PEM format.
    ssl_certificate_key                             /etc/ssl/private/plc-6486b4e.reactor.norzh.nuclea_rsa.key;             # Secret key in PEM format.
    ssl_trusted_certificate                         /etc/ssl/private/plc-6486b4e.reactor.norzh.nuclea_chain.pem;           # Certificate chain.

    ssl_ciphers                                     '!EDH:!EXP:!SHA:!DSS:EECDH+aRSA+AESGCM:EECDH+aRSA+SHA384:EECDH+aRSA+SHA256';  # Cipher suite (see https://tls.imirhil.fr/suite/ and `openssl ciphers | sed -r -e 's/:/\n/g'`).
    ssl_prefer_server_ciphers                       on;                                                 # Specifies that server ciphers should be preferred over client ciphers.

    # ssl_dhparam                                     /etc/ssl/private/private/dh4096.pem;                # Diffie-Hellman server params with 4096 bits (generated using `openssl dhparam 4096 -out /etc/ssl/private/private/dh4096.pem`).
    ssl_ecdh_curve                                  sect571r1:secp521r1:brainpoolP512r1:secp384r1;      # Elliptic Curve Diffie-Hellman server params.

    ssl_session_cache                               shared:SSL:10m;                                     # Create a shared cache able to store about 80000 sessions (about 4000 for 1MB storage).
    ssl_session_timeout                             5m;                                                 # Timeout before session to be dropped.
    ssl_session_tickets                             off;                                                # Disable TLS session tickets.
}


