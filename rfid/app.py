#!/usr/bin/env python2
from smartcard.System import readers
import vobject
import time
import os
import re

SECTOR_START = 1
SECTOR_END = 5

def list_to_hex_string(lst):
	return '0x%s' % ''.join(map(lambda x: hex(x)[2:], list(lst)))

def list_to_string(lst):
	return ''.join(map(lambda x: chr(x), list(lst)))

def decrypt_sector(connection, sector):
	COMMAND = [0xFF, 0x86, 0x00, 0x00, 0x05, 0x01, 0x00, sector*4, 0x61, 0x00]  # decrypt first block of the sector.
	data, sw1, sw2 = connection.transmit(COMMAND)

	if (sw1, sw2) == (0x90, 0x0):
		return True
	else:
		return False

def get_uid(connection):
	COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00]
	data, sw1, sw2 = connection.transmit(COMMAND)

	return int(list_to_hex_string(data), 16)

def main():
	os.system('reset && clear')

	reader = readers()[0]
	print('Using %s' % reader)

	prev_uid = 0
	while True:
		card_data = ''
		try:
			connection = reader.createConnection()
			connection.connect()
		except:
			prev_uid = 0
			continue
		else:
			try:
				uid = get_uid(connection)
				if prev_uid != uid:
					prev_uid = uid

					print('New card: 0x%x' % uid)

					for sector in range(SECTOR_START, SECTOR_END, 1):
						print('Decrypting sector #%d' % sector)
						decrypt_sector(connection, sector)

						for block in range(sector*4, sector*4+4):
							COMMAND = [0xFF, 0xB0, 0x00, block, 16]
							data, sw1, sw2 = connection.transmit(COMMAND)
							if (sw1, sw2) == (0x90, 0x0):
								if block % 4 != 3:
									card_data += list_to_string(data)
							else:
								print('Failed to read data on block #%d' % block)
		
					match = re.search(r'(BEGIN:VCARD.+END:VCARD)', card_data, re.MULTILINE|re.DOTALL)
					if match:
						email = vobject.readOne(match.group(0)).contents['email'][0].value
						name = vobject.readOne(match.group(0)).contents['fn'][0].value
						if re.match(r'^[A-Za-z0-9\-]+\@norzh\.nuclea$', email):
							print('ACCESS GRANTED')
							if email == 'test@norzh.nuclea' or 'test' in name.lower():
								print('Testing account detected! But here is your flag: %s' % 'ENSIBS{R34d_tH3_rUl3s!_N0_t3st1nG_4Cc0uN7!}')
							else:
								print('Welcome %s!' % name.split(' ')[0])
								print('Here is your flag: %s' % 'ENSIBS{M1f4r3_Cl45siC...}')
			except Exception as e:
				prev_uid = 0
				print(e)
				pass
			finally:
				connection.disconnect()
				time.sleep(5)
				os.system('reset && clear')
				print('Waiting for a card...')

if __name__ == "__main__":
	main()
