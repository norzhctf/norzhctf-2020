#!/usr/bin/env python3
# -*- coding: latin-1 -*-
from collections import deque

def bin_str_to_list(state):
    '''
    Get a bit list from a binary string.
    '''
    return list(map(lambda x: int(x), list(state)))

def bin_list_to_str(state):
    '''
    Get a binary string from a bit list.
    '''
    return ''.join(map(lambda x: str(int(x)), state)).zfill(4)

def byte_to_nibbles(byte):
    '''
    Split the byte into high and low nibbles.
    '''
    return (byte >> 4, byte & 0x0F)

def CX(t, c):
    '''
    Pauli gate / Controlled-X gate.
    '''
    t ^= c
    return (t, c)

def reverse_CX(t, c):
    '''
    Reverse Pauli gate / Reverse controlled-X gate.
    '''
    return (CX(t, not c)[0], c)

def CCX(t, c1, c2):
    '''
    Toffoli gate / Controlled-Controlled-X gate.
    '''
    return (CX(t, (c1 & c2))[0], c1, c2)

def CSWAP(t1, t2, c):
    '''
    Fredkin gate / Controlled-SWAP.
    '''
    t1, t2 = CX(t1, t2)
    t2, t1, c = CCX(t2, t1, c)
    t1, t2 = CX(t1, t2)
    return (t1, t2, c)

def encode(state):
    if len(state) == 4:
        state = deque(bin_str_to_list(state))  # 

        # - reverse CNOT / reverse CX - #
        state[1], state[0] = reverse_CX(state[1], state[0])

        # - CNOT / CX - #
        state[2], state[1] = CX(state[2], state[1])

        # - CNOT / CX - #
        state[3], state[2] = CX(state[3], state[2])

        # - CCNOT / CCX - #
        state[1], state[0], state[2] = CCX(state[1], state[0], state[2])

        # SWAP(t1, t4); SWAP(t2, t4); SWAP(t3, t4) = left rotate
        state.rotate(1)

        # - reverse CNOT / reverse CX - #
        state[3], state[1] = reverse_CX(state[3], state[1])

        # - CSWAP - #
        state[0], state[1], state[2] = CSWAP(state[0], state[1], state[2])

        return state

def decode(state):
    if len(state) == 4:
        state = deque(bin_str_to_list(state))

        # - CSWAP - #
        state[0], state[1], state[2] = CSWAP(state[0], state[1], state[2])

        # - reverse CNOT / reverse CX - #
        state[3], state[1] = reverse_CX(state[3], state[1])

        # SWAP(t4, t1); SWAP(t3, t1); SWAP(t2, t1) = right rotate
        state.rotate(-1)

        # - CCNOT / CCX - #
        state[1], state[0], state[2] = CCX(state[1], state[0], state[2])

        # - CNOT / CX - #
        state[3], state[2] = CX(state[3], state[2])

        # - CNOT / CX - #
        state[2], state[1] = CX(state[2], state[1])

        # - reverse CNOT / reverse CX - #
        state[1], state[0] = reverse_CX(state[1], state[0])

        return state

def main():
    # flag = 'ENSIBS{qu4ntUm_G4tEs_w1ThOU7_SuPErp0si71oN}'
    # buffer = list()
    # for i in range(len(flag)):  # for each bytes
    #     buffer.append('')  # add an entry to the buffer
    #     byte = ord(flag[i])  # get byte
    #     nibbles = byte_to_nibbles(byte)  # split the byte into high and low nibbles
    #     for j in range(2):  # for each nibbles
    #         state = bin(nibbles[j])[2:].zfill(4)  # convert the nibble to a binary string
    #         out_state = encode(state)  # get the output state of decoder
    #         buffer[i] += bin_list_to_str(out_state)  # add the nibble to buffer
    # flag = list(map(lambda b: chr(int(b, 2)), buffer))  # for each bytes in buffer, get its char value
    # with open('msg.bin', 'wt', encoding='latin-1') as fd:
    #     fd.write(''.join(flag))

    with open('msg.bin', 'rt', encoding='latin-1') as fd:
        flag_enc = fd.read()
        buffer = list()
        for i in range(len(flag_enc)):  # for each bytes
            buffer.append('')  # add an entry to the buffer
            byte = ord(flag_enc[i])  # get byte
            nibbles = byte_to_nibbles(byte)  # split the byte into high low nibbles
            for j in range(2):  # for each nibbles
                state = bin(nibbles[j])[2:].zfill(4)  # convert the nibble to a binary string
                out_state = decode(state)  # get the output state of decoder
                buffer[i] += bin_list_to_str(out_state)  # add the nibble to buffer
        flag = list(map(lambda b: chr(int(b, 2)), buffer))  # for each bytes in buffer, get its char value
        print(''.join(flag))

if __name__ == "__main__":
    main()