# NorzhCTF 2020

## Description

## Network

### Diagram

![network diagram](docs/network_diagram.png)

![terraform graph](docs/terraform_graph.svg)

### Services

| Category       | Frontend                                          | Host                          | IP             |
|----------------|---------------------------------------------------|-------------------------------|----------------|
| WEB            | [Honeypot 1](http://10.13.0.1)                    | `wz-xx-honey1 (Honeypot #1)`  | `10.13.0.1`    |
| WEB            | [Honeypot 2](http://10.13.38.1)                   | `wz-xx-honey2 (Honeypot #2)`  | `10.13.38.1`   |
| WEB            | [Website](https://www.norzh.nuclea)               | `wz-xx-extranet (Extranet)`   | `10.13.18.165` |
| WEB            | [Intranet](https://intranet.norzh.nuclea)         | `wz-xx-intranet (Server)`     | `10.13.42.63`  |
| Forensic       | [Lab](https://lab.norzh.nuclea)                   | `wz-xx-forensic (Server)`     | `10.13.42.64`  |
| Crypto/Reverse | [Git](https://git.norzh.nuclea)                   | `wz-xx-git (Server)`          | `10.13.42.65`  |
| WEB            | [Passgen](https://passgen.norzh.nuclea)           | `wz-xx-web (Server)`          | `10.13.42.66`  |
| WEB            | [Camera](http://cam.blueteam.norzh.nuclea)        | `wz-xx-cam (Blueteam)`        | `10.13.49.56`  |
| WEB            | [Grafana](https://grafana.reactor.norzh.nuclea)   | `wz-xx-plc-ctl (Reactor CTL)` | `10.13.51.69`  |
| WEB            | [PLC CTL](https://plc-ctl.reactor.norzh.nuclea)   | `wz-xx-plc-ctl (Reactor CTL)` | `10.13.51.69`  |
| WEB            | [Alarm](https://plc-6486b4e.reactor.norzh.nuclea) | `wz-xx-plc1 (Reactor)`        | `10.13.67.33`  |
 
## Deployment

### Docker Compose

Afin de tester les différents challenges localement avant déploiement et composer les images associées aux services, l'utilisation de `docker-compose` est recommandée.

Composition des images associées aux services et téléchargement sur un registre d'image&nbsp;:

```bash
cd category/challenge/docker/
docker-compose pull   # téléchargement de l'image depuis le registre
docker-compose build  # composition d'une nouvelle image locale
docker-compose push   # téléchargement de l'image locale vers le registre
```

Lancement des services&nbsp;:

```bash
docker-compose up -d
```

Affichage des logs associés au service&nbsp;:

```bash
docker-compose logs -f
```

**OU**, lancement des services dans un screen (suivi des logs plus rapide)&nbsp;:

```bash
source .env  # récupération du nom du challenge depuis un fichier de variable
screen -dmS ${COMPOSE_PROJECT_NAME} docker-compose logs -f  # affichage des logs dans un screen
screen -ls  # liste des screens ouverts
screen -r ${COMPOSE_PROJECT_NAME}  # attachement au screen
```

### Terraform

L'utilisation de `docker-compose` pour le déploiement de challenges sur différents serveurs peut s'avérer chronophage.

En effet, il est tout d'abord nécessaire de se connecter au serveur distant (`ssh`), d'y installer `docker-compose`, d'y déposer les fichiers,
de télécharger les images (éventuellement de les composer), démarrer les services et en cas de panne, répéter ces différentes étapes.

En utilisant Terraform, il est possible d'automatiser en grande partie ce travail de déploiement et d'administration des services.

Déploiement d'un challenge (adapter les fichiers `docker.tf` et `terraform.tfvars.json` au besoin)&nbsp;:

```bash
cd category/challenge/deploy/
make  # déploiement du challenge X sur le serveur X. Ces paramètres sont indiqués dans les fichiers terraform.tfvars.json et docker.tf
```

Suppression d'un challenge&nbsp;:

```bash
make clean  # suppression du challenge X du serveur X. Ces paramètres sont indiqués dans les fichiers terraform.tfvars.json et docker.tf
```

Démonstration&nbsp;:

![terraform](docs/terraform.png)

## Mise en place du CTFd

Clonez le projet `norzhctf/norzhctf-2020-ctfd`&nbsp;:

```bash
mkdir -p /opt/norzhctf/infra/
pushd /opt/norzhctf/ctfd/infra/
git clone https://gitlab.com/norzhctf/norzhctf-2020-ctfd ctfd
```

Démarrez les services&nbsp;:

```bash
pushd ctfd/
docker-compose up -d
```

Accédez au CTFd, créez un compte admin et accédez aux configurations&nbsp;:

![CTFd appearance](docs/ctfd_appearance.png)

Cette page de configuration permet d'ajouter des styles aux pages publiques (hors mode admin) et sélectionner le thème à utiliser.

Le thème choisi ici est le thème `aperi` (designé par [Alex GARRIDO](https://twitter.com/Zeecka_) et adapté pour le NorzhCTF) et les styles appliqués sont les suivants&nbsp;:

```css
.ctf_logo {
    width: 150px;
    height: 100%;
}
table a,
.jumbotron,
a {
    color: #A80101;
}

.navbar a.nav-link {
    font-size: 13pt;
    color: #333438;
}

.navbar a.nav-link:hover {
    color: #A80101;
}

.solved-challenge {
    background-color: #A80101 !important;
}
```

Quelques pages ont également été modifiées et ajoutées au CTFd&nbsp;:

![ctfd pages](docs/ctfd_pages.png)

La page d'accueil est affichée aux joueurs authentifiés ou non ([sources](docs/ctfd_homepage.html))&nbsp;:

![ctfd homepage](docs/ctfd_homepage.png)

La page `règlement` contient un extrait du règlement du CTF ([sources](docs/ctfd_rules.html))&nbsp;:

![ctfd rules](docs/ctfd_rules.png)

Les dernières étapes consistent à ajouter les différents challenges à la plateforme, créer les équipes des futurs joueurs et programmer les heures de début et de fin de la compétition.

![ctfd challenges](docs/ctfd_challenges.png)
