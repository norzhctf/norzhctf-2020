# Watcher processus

import os
import time
import psutil
import socket

PROCESS_NAME = 'Socket_gcc.exe'
PROCESS_PATH = f"bin\debug\{PROCESS_NAME}"

def respawn():
    # Kill existing processes at startup
    print("Respawning !")
    for p in psutil.process_iter():
        if(p.name() == PROCESS_NAME):
            os.system(f'taskkill /f /im {PROCESS_NAME}')
    pid  = os.spawnl(os.P_NOWAIT, PROCESS_PATH, PROCESS_PATH)

while True:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('127.0.0.1',8888))
        sock.close()
    except:
        respawn()
    time.sleep(10)
