# Nom du challenge

Les administrateurs de NorzhNuclea ont retrouvé un serveur enfoui dans leur parc. Ils n'ont plus les identifiants.
Cependant, il ont retrouvé le binaire d'un des services qui tourne sur la machine.
Exploitez le service pour reprendre la main sur le serveur.

OS :  Windows
Anti-virus : Désactivé

[Areizen](https://twitter.com/Areizen_)

## Ressources du challenge

- FILENAME : [service.exe](../files/service.exe)
- IP: w.x.y.z

## Hints

None
