#!/bin/bash

echo "Killing autoheal"
screen -S heal -X quit

echo "Stopping Indus containers"
pushd indus/alarm/docker; docker-compose down; popd

echo "Stopping Reverse containers"
pushd reverse/door_lock/docker; docker-compose down; popd

echo "Stopping Web containers"
pushd web/intranet/docker; docker-compose down; popd
pushd web/ip_camera/docker; docker-compose down; popd
pushd web/passgen/docker; docker-compose down; popd
pushd web/system_alive_checker/docker; docker-compose down; popd

echo "All containers should be down (please check!)"
screen -ls
