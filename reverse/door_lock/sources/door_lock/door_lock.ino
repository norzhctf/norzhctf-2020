#define BLINK(pin, time) \
        digitalWrite(pin, HIGH); \
        delay(time); \
        digitalWrite(pin, LOW); \
        delay(time);

// BUTTON
int conf_pin = 7;   // confirm button PIN
int rst_pin = 8;    // reset button PIN
int but1_pin = 10;  // digit #1
int but2_pin = 11;  // digit #2
int but3_pin = 12;  // digit #3
int but4_pin = 13;  // digit #4

// LED
int success_pin = 4;
int error_pin = 5;

// RELAY
int door_pin = 6;

int digit_ctr = 0;
int digit = 0;
int user_code[7];
int code[] = {2, 4, 2, 1, 3, 2, 4};

void setup() {
  // setup input pins.
  pinMode(conf_pin, INPUT_PULLUP);
  pinMode(rst_pin, INPUT_PULLUP);
  pinMode(but1_pin, INPUT_PULLUP);
  pinMode(but2_pin, INPUT_PULLUP);
  pinMode(but3_pin, INPUT_PULLUP);
  pinMode(but4_pin, INPUT_PULLUP);

  // setup output pins.
  pinMode(success_pin, OUTPUT);
  pinMode(error_pin, OUTPUT);
  pinMode(door_pin, OUTPUT);

  // set relay to up.
  digitalWrite(door_pin, HIGH);
}

void loop() {
  digit = 0;
  if (digitalRead(but1_pin) == HIGH) {
    digit = 1;
  } else if (digitalRead(but2_pin) == HIGH) {
    digit = 2;
  } else if (digitalRead(but3_pin) == HIGH) {
    digit = 3;
  } else if (digitalRead(but4_pin) == HIGH) {
    digit = 4;
  } else if (digitalRead(conf_pin) == HIGH) {
    if (check_password() == 0) {
  	  BLINK(error_pin, 30)
    	BLINK(error_pin, 30)
    } else {
    	BLINK(success_pin, 30)
    	BLINK(success_pin, 30)
      digitalWrite(door_pin, LOW);
    	delay(100);
    	digitalWrite(door_pin, HIGH);
    }
    digit_ctr = 0;
  } else if (digitalRead(rst_pin) == HIGH) {
    BLINK(error_pin, 30)
    BLINK(success_pin, 30)
    BLINK(error_pin, 30)
    BLINK(success_pin, 30)
    digit_ctr = 0;
  }

  if (digit != 0) {
  	user_code[digit_ctr] = digit;
    BLINK(success_pin, 50)
    digit_ctr++;
  }
}

int check_password() {
  for (int i=0; i<(sizeof(code)/sizeof(int)); i++) {
    if (user_code[i] != code[i]) {
      return 0;
    }
  }
  return 1;
}
