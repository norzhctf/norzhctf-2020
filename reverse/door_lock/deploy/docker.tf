# Create a new variable
variable "network" {
  type = object({
    domain_name = string,
    dns = list(string)
  })
}

variable "chall_event" {
  type = string
  description = "The event where the challenge will be played on."
}

variable "chall_id" {
  type = string
  description = "The id of the challenge to deploy."
}

variable "chall_host" {
  type = object({
    protocol = string
    username = string
    hostname = string
    port = number
    private_key = string
  })
  default = {
    protocol = "ssh"
    username = "root"
    hostname = ""
    port = 22
    private_key = ".ssh/id_rsa"
  }
  description = "The id of the challenge to deploy."
}

# Copy docker files to the remote server
resource "null_resource" "docker_file_provisionner" {
  connection {
    type = "${var.chall_host.protocol}"
    user = "${var.chall_host.username}"
    host = "${var.chall_host.hostname}"
    port = "${var.chall_host.port}"
    private_key = file("${var.chall_host.private_key}")
    agent = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }

  provisioner "file" {
    source = "../docker/conf"
    destination = "/opt/${var.chall_event}/${var.chall_id}/conf"
  }

  provisioner "file" {
    source = "../docker/data"
    destination = "/opt/${var.chall_event}/${var.chall_id}/data"
  }

  provisioner "file" {
    source = "../docker/log"
    destination = "/opt/${var.chall_event}/${var.chall_id}/log"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod -R 777 /opt/${var.chall_event}/${var.chall_id}/log /opt/${var.chall_event}/${var.chall_id}/data/gogs"
    ]
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "rm -rf /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }
}

# Configure a docker provider
provider "docker" {
  host = "${var.chall_host.protocol}://${var.chall_host.username}@${var.chall_host.hostname}:${var.chall_host.port}"
}

# Create a new docker network
resource "docker_network" "front" {
  name = "${var.chall_id}-front"
  driver = "bridge"
}

resource "docker_network" "back" {
  name = "${var.chall_id}-back"
  driver = "bridge"
  internal = true
}

# Create a new docker container
resource "docker_container" "gogs" {
  image = "${docker_image.gogs.latest}"
  name = "${var.chall_id}-gogs"
  hostname = "${var.chall_id}-gogs"
  domainname = "${var.network.domain_name}"
  ports { # 127.0.0.1:3000:3000/tcp
    ip = "0.0.0.0"
    external = "3000"
    internal = "3000"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/gogs"
    container_path = "/data"
    read_only = false
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "http" {
  image = "${docker_image.proxy.latest}"
  name = "${var.chall_id}-proxy"
  hostname = "${var.chall_id}-proxy"
  domainname = "${var.network.domain_name}"
  network_mode = "host"
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/nginx.conf"
    container_path = "/etc/nginx/nginx.conf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/conf.d"
    container_path = "/etc/nginx/conf.d"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/certs"
    container_path = "/etc/ssl/private"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/log/nginx"
    container_path = "/usr/share/nginx/log"
    read_only = false
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

resource "docker_image" "gogs" {
  name = "gogs/gogs:latest"
}

resource "docker_image" "proxy" {
  name = "creased/webdev-env-http:latest"
}