#include <unicorn/unicorn.h>
#include <string.h>

// ENSIBS{!u4r3S0L3et!}

// Defining some emulation constants
#define ADDRESS 0x1000000
#define CODE_SIZE  2 * 1024 * 1024
#define STACK_ADDR  CODE_SIZE + ADDRESS
#define STACK_SIZE  2 * 1024 * 1024

void hook_intr(uc_engine *uc, uint32_t intno, void *user_data){
   char a[20];
   int r_esp, r_eip, r_eax;
   uc_reg_read(uc, UC_X86_REG_ESP, &r_esp);
   if(intno == 0x13){
      scanf("%20s", a);
      uc_mem_write(uc, r_esp, a, 20);
   }else if (intno == 0x37){
      printf("WELL DONE Captain !");
      system("/bin/bash");
   }else if(intno == 0x34){
      uc_mem_read(uc, r_esp, a, 8);
      // ENSIBS{!
      if(strcmp("ENSIBS{!",a) != 0){
         printf("Failed !\n");
         exit(-1);
      }
   }else if(intno == 0x21){
      // Here is xored with eax -100
      char test[] = "\x9c\xdd\xbd\xde\xa2";
      // r3S0L
      uc_reg_read(uc, UC_X86_REG_EAX, &r_eax);
      uc_mem_read(uc, r_esp + 10, a, 5);
      for(int i=0; i<5; i++){
        if((char)(a[i] ^ (r_eax - 100)) != (char)(test[i]) ){
            printf("Failed !\n");
            exit(-1);
        }
      }
   }else if(intno == 0x0){
      // Juste to add new interrupt that is not in the code
      uc_mem_write(uc,ADDRESS, "\x83\xC4\x10\xCD\xAA", 5);
      uc_emu_start(uc, ADDRESS, ADDRESS + 5 - 1, 0, 0);   

   }else if(intno == 0xaa){
      // xor between code and input
      char tmp_[] = "\xfe\xcf\x71\x10\xbd";
      char tmp [] = "\x00\x00\x00\x00\x00";

      uc_mem_read(uc, 0x1300000 + 15, a,5);
      uc_mem_read(uc,ADDRESS + 3,tmp, 5);
      
      for(int i=0; i<5;i++){
         if((char)(a[i]^tmp[i]) != (char)tmp_[i]){
            printf("Failed !\n");
            exit(-1);
         }
       
      }

      // Now it's time to think, let's build an int 0x37 (\xcd\x37)
      char code[] = "\xb8\x03";
      uc_mem_read(uc, 0x1300008, a,2);
      code[0] ^= a[0] & 0xff; 
      code[1] ^= a[1] & 0xff; 
      uc_mem_write(uc,ADDRESS, code, 2);
      uc_emu_start(uc, ADDRESS, ADDRESS + 5 - 1, 0, 0);   
   
   }else{
      printf("Failed\n",intno);
      exit(-1);
   }
}
 
int main(int argc, char **argv, char **envp)
{
   uc_engine *uc;
   uc_err err;
   uc_hook hook;
   #ifndef DEBUG
   __asm__ volatile (
      "call .+5\n\t"
      "add dword ptr[rsp], 5\n\t"
      "ret\n\t"
      );
   #endif
   err = uc_open(UC_ARCH_X86, UC_MODE_32, &uc);

   /*
   xor edx, edx
   mov dx, 0x05eb
   xor eax, eax
   jz .-4
   .byte 0xe8
   call .+5
   add dword ptr [esp], 5
   ret
   mov ecx, 8897
   jz .+5
   jnz .+3
   .byte 0xe8
   nop
   xor eax, eax
   jz beer+1
       jnz beer+1
   beer:
   .byte 0xe8
   add eax,338
   int 0x13
   int 0x34
   xor eax, eax
   jz beer2+1
       jnz beer2+1
   beer2:
   .byte 0xe8
   add eax,338
   int 0x21
   int 0x20
   */
   char format[] = {
      49,210,102,186,235,
      5,49,192,116,250,232,
      232,0,0,0,0,131,4,36,
      5,195,185,193,34,0,0,
      116,3,117,1,232,144,
      49,192,116,3,117,1,
      232,5,82,1,0,0,205,19,
      205,52,49,192,116,
      3,117,1,232,5,82,
      1,0,0,205,33,205,
      32,};
   
   int length = 64;

   //mapping useful sections
   uc_mem_map(uc, ADDRESS, CODE_SIZE, UC_PROT_ALL);
   uc_mem_map(uc, STACK_ADDR, STACK_SIZE, UC_PROT_ALL);

   // esp need to be placed in stack
   int r_esp = STACK_ADDR+STACK_SIZE/2;
   uc_reg_write(uc,UC_X86_REG_ESP, &r_esp);

   // writing code to code space
   uc_mem_write(uc, ADDRESS, format, length - 1);

   // Adding interrupt hook
   uc_hook_add(uc, &hook, UC_HOOK_INTR, hook_intr, NULL, 1, 0);
   uc_emu_start(uc, ADDRESS, ADDRESS + length - 1, 0, 0);
 
   return 0;
}