import hashlib
import string
import random

def create_seed(username, date):
    """create a seed for username and date."""
    return int(hashlib.sha256(username.encode()).hexdigest(), 16) + int(date)

ALPHABET = string.ascii_letters + string.digits + '!@#$%^&*()'
LENGTH = 16

username = 'bmoine'
date = 1574950229899 // 100000

rand = random.Random()
seed = create_seed(username, date)
rand.seed(seed)

print(''.join(rand.choice(ALPHABET) for _ in range(LENGTH)))