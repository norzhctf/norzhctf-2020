#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, jsonify, request, make_response)
from .utils import (generate_password)

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
def index():
    response = render_template('index.html'), 200
    return response

@main.route('/password', methods=['POST'])
def password():
    username = request.form.get('user')
    timestamp = request.form.get('ts')
    response = make_response(generate_password(username, timestamp), 200)
    response.mimetype = 'text/plain'
    return response
