#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime

from .config import (APP_SECRET_KEY)
from flask import Flask

def create_app():
    app = Flask(__name__)

    config = {
        'DEBUG': False,
        'SECRET_KEY': APP_SECRET_KEY,
    }

    app.config.from_mapping(config)
    app.url_map.strict_slashes = False

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
