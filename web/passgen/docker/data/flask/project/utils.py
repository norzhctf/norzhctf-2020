#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import hashlib
import string
import random

def create_seed(username, date):
    """create a seed for username and date."""
    return int(hashlib.sha256(username.encode()).hexdigest(), 16) + int(date)

def generate_password(username, timestamp):
    alphabet = string.ascii_letters + string.digits + '!@#$%^&*()'
    length = 16
    date = int(timestamp)
    if date > int(1e7):
        if date >= int(10e11):
            date = date // 100000

        rand = random.Random()
        seed = create_seed(username, date)
        rand.seed(seed)

        return (''.join(rand.choice(alphabet) for _ in range(length)))