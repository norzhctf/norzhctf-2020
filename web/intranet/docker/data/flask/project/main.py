#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, jsonify, request)
from flask_login import (login_required, current_user)
from . import (db)

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
@login_required
def index():
    response = render_template('index.html', page='Intranet'), 200
    return response
