#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, redirect, url_for, request, flash)
from flask_login import (login_user, logout_user, login_required)
from werkzeug.security import (generate_password_hash, check_password_hash)
from .models import User
from . import db

auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['GET'])
def login():
    return render_template('login.html', page='Login'), 200

@auth.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    response = redirect(url_for('main.index'))
    return response

@auth.route('/login', methods=['POST'])
def login_post():
    username = request.form.get('username')
    password = request.form.get('password')

    user = User.query.filter_by(username=username).first()

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not check_password_hash(user.password, password): 
        flash('Please check your login details and try again.')
        response = redirect(url_for('auth.login')) # if user doesn't exist or password is wrong, reload the page
    else:
        login_user(user)
        response = redirect(url_for('main.index'))

    return response
