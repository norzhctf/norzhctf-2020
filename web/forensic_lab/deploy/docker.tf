# Create a new variable
variable "network" {
  type = object({
    domain_name = string,
    dns = list(string)
  })
}

variable "chall_event" {
  type = string
  description = "The event where the challenge will be played on."
}

variable "chall_id" {
  type = string
  description = "The id of the challenge to deploy."
}

variable "chall_host" {
  type = object({
    protocol = string
    username = string
    hostname = string
    port = number
    private_key = string
  })
  default = {
    protocol = "ssh"
    username = "root"
    hostname = ""
    port = 22
    private_key = ".ssh/id_rsa"
  }
  description = "The id of the challenge to deploy."
}

# Copy docker files to the remote server
resource "null_resource" "docker_file_provisionner" {
  connection {
    type = "${var.chall_host.protocol}"
    user = "${var.chall_host.username}"
    host = "${var.chall_host.hostname}"
    port = "${var.chall_host.port}"
    private_key = file("${var.chall_host.private_key}")
    agent = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }

  provisioner "file" {
    source = "../docker/conf"
    destination = "/opt/${var.chall_event}/${var.chall_id}/conf"
  }

  provisioner "file" {
    source = "../docker/data"
    destination = "/opt/${var.chall_event}/${var.chall_id}/data"
  }

  provisioner "file" {
    source = "../docker/log"
    destination = "/opt/${var.chall_event}/${var.chall_id}/log"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod -R 777 /opt/${var.chall_event}/${var.chall_id}/log /opt/${var.chall_event}/${var.chall_id}/data/www/uploads"
    ]
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "rm -rf /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }
}

# Configure a docker provider
provider "docker" {
  host = "${var.chall_host.protocol}://${var.chall_host.username}@${var.chall_host.hostname}:${var.chall_host.port}"
}

# Create a new docker network
resource "docker_network" "front" {
  name = "${var.chall_id}-front"
  driver = "bridge"
}

resource "docker_network" "back" {
  name = "${var.chall_id}-back"
  driver = "bridge"
  internal = true
}

# Create a new docker container
resource "docker_container" "fpm" {
  image = "${docker_image.fpm.latest}"
  name = "${var.chall_id}-fpm"
  hostname = "${var.chall_id}-fpm"
  domainname = "${var.network.domain_name}"
  ports { # 0.0.0.0:80:80/tcp
    ip = "0.0.0.0"
    external = "80"
    internal = "80"
    protocol = "tcp"
  }
  ports { # 0.0.0.0:443:443/tcp
    ip = "0.0.0.0"
    external = "443"
    internal = "443"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/nginx.conf"
    container_path = "/etc/nginx/nginx.conf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/conf.d"
    container_path = "/etc/nginx/conf.d"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/php/php-fpm.conf"
    container_path = "/usr/local/etc/php-fpm.conf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/certs"
    container_path = "/etc/ssl/private"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/log/nginx"
    container_path = "/usr/share/nginx/log"
    read_only = false
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/www"
    container_path = "/usr/share/nginx/html"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/www/uploads"
    container_path = "/usr/share/nginx/html/uploads"
    read_only = false
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/flags/root_flag.txt"
    container_path = "/root/flag.txt"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/flags/user_flag.txt"
    container_path = "/user_flag.txt"
    read_only = true
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "healthcheck" {
  image = "${docker_image.curl.latest}"
  name = "${var.chall_id}-healthcheck"
  hostname = "${var.chall_id}-healthcheck"
  domainname = "${var.network.domain_name}"
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  command = ["/bin/watch", "-n", "5", "--", "curl", "-u", "'web-dev:ENSIBS{I_kn0w_Ur_passWord_Web_dev}'", "http://www.norzh.nuclea:3000/health"]
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

resource "docker_image" "fpm" {
  name = "creased/fpm:latest"
}

resource "docker_image" "curl" {
  name = "creased/alpine:curl"
}