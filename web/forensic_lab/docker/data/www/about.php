<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>File Upload | Norzh Nuclea</title>
		<link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
		<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
	</head>
	<body>
		<div class="container">
			<header class="theme-header">
				<h1>File Upload</h1>
				<p>The smartest way to upload your files by <strong> Norzh Nuclea</strong>.</p>
			</header>
			<div class="content">

				<div>
					<?php 
						phpinfo(1);
					?>
				</div>
				<footer>Norzh Nuclea CTF 2020 by ENSIBS</footer>
			</div>
		</div>
		<script src="assets/js/custom-file-input.js"></script>
	</body>
</html>


