<!DOCTYPE html>
<html lang="en" class="no-js">
        <head>
                <meta charset="UTF-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>File Upload | Norzh Nuclea</title>
                <link rel="stylesheet" type="text/css" href="assets/css/normalize.css" />
                <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
                <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
        </head>
        <body>
                <div class="container">
                        <header class="theme-header">
                                <h1>File Upload</h1>
                                <p>The smartest way to upload your files by <strong> Norzh Nuclea</strong>.</p>
                        </header>
                        <div class="content">

                                <div class="box">
<?php
foreach($_FILES['userfile']['error'] as $k=>$v)
 {
    $uploadfile = 'uploads/'. basename($_FILES['userfile']['name'][$k]) . '.safe';
    if (move_uploaded_file($_FILES['userfile']['tmp_name'][$k], $uploadfile))
    {
        echo "<p> <strong>File</strong> : ", '<a href="' . $uploadfile . '">'  , htmlspecialchars($_FILES['userfile']['name'][$k]) , '</a>' ," is valid, and was successfully uploaded.</p>";
    }
    else
    {
        echo "<p> <strong>File</strong> : ", htmlspecialchars($_FILES['userfile']['name'][$k]), " upload attack!</p>";
    }
}
?>				
				<a href="/" class="new-upload">Upload Again</a>
                                </div>
                                <footer>Norzh Nuclea CTF 2020 by ENSIBS</footer>
                        </div>
                </div>
                <script src="assets/js/custom-file-input.js"></script>
        </body>
</html>

