#!/bin/bash

###
# fix permissions
#
chown -R www-data:www-data /usr/share/nginx/


###
# nginx
#
nginx -g "daemon on;"

###
# php fpm
#
php-fpm -D &

##
# logs
#
tail -F /usr/share/nginx/log/*.log

