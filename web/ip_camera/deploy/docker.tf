# Create a new variable
variable "network" {
  type = object({
    domain_name = string,
    dns = list(string)
  })
}

variable "chall_event" {
  type = string
  description = "The event where the challenge will be played on."
}

variable "chall_id" {
  type = string
  description = "The id of the challenge to deploy."
}

variable "flask" {
  type = object({
    env = object({
      APP_HOST = string
      APP_PORT = number
      APP_SECRET_KEY = string
      MQTT_BROKER_HOST = string
      MQTT_BROKER_PORT = number
      MQTT_TOPIC = string
    })
  })
}

variable "cam1" {
  type = object({
    env = object({
      CAMERA_NAME = string
      BASE_IMG_FOLDER = string
      BASE_IMG_FILE = string
      NEW_IMG_FOLDER = string
      TEXT_FONT = string
      REFRESH_TIME = number
      MQTT_BROKER_HOST = string
      MQTT_BROKER_PORT = number
      MQTT_BROKER_TRANSPORT = string
      MQTT_TOPIC = string
    })
  })
}

variable "cam2" {
  type = object({
    env = object({
      CAMERA_NAME = string
      BASE_IMG_FOLDER = string
      BASE_IMG_FILE = string
      NEW_IMG_FOLDER = string
      TEXT_FONT = string
      REFRESH_TIME = number
      MQTT_BROKER_HOST = string
      MQTT_BROKER_PORT = number
      MQTT_BROKER_TRANSPORT = string
      MQTT_TOPIC = string
    })
  })
}

variable "chall_host" {
  type = object({
    protocol = string
    username = string
    hostname = string
    port = number
    private_key = string
  })
  default = {
    protocol = "ssh"
    username = "root"
    hostname = ""
    port = 22
    private_key = ".ssh/id_rsa"
  }
  description = "The id of the challenge to deploy."
}

# Copy docker files to the remote server
resource "null_resource" "docker_file_provisionner" {
  connection {
    type = "${var.chall_host.protocol}"
    user = "${var.chall_host.username}"
    host = "${var.chall_host.hostname}"
    port = "${var.chall_host.port}"
    private_key = file("${var.chall_host.private_key}")
    agent = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }

  provisioner "file" {
    source = "../docker/conf"
    destination = "/opt/${var.chall_event}/${var.chall_id}/conf"
  }

  provisioner "file" {
    source = "../docker/data"
    destination = "/opt/${var.chall_event}/${var.chall_id}/data"
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "rm -rf /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }
}

# Configure a docker provider
provider "docker" {
  host = "${var.chall_host.protocol}://${var.chall_host.username}@${var.chall_host.hostname}:${var.chall_host.port}"
}

# Create a new docker network
resource "docker_network" "front" {
  name = "${var.chall_id}-front"
  driver = "bridge"
}

resource "docker_network" "back" {
  name = "${var.chall_id}-back"
  driver = "bridge"
  internal = true
}

# Create a new docker container
resource "docker_container" "flask" {
  image = "${docker_image.flask.latest}"
  name = "${var.chall_id}-flask"
  hostname = "${var.chall_id}-flask"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.flask.env): "${env}=${lookup(var.flask.env, env)}"]
  ports { # 127.0.0.1:8000:8000/tcp
    ip = "0.0.0.0"
    external = "${var.flask.env.APP_PORT}"
    internal = "${var.flask.env.APP_PORT}"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/flask"
    container_path = "/data"
    read_only = true
  }
  command = ["python3", "app.py"]
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "${var.flask.env.APP_PORT}"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "mqtt" {
  image = "${docker_image.mqtt.latest}"
  name = "${var.chall_id}-mqtt"
  hostname = "${var.chall_id}-mqtt"
  domainname = "${var.network.domain_name}"
  ports { # 0.0.0.0:1883:1883/tcp
    ip = "0.0.0.0"
    external = "1883"
    internal = "1883"
    protocol = "tcp"
  }
  ports { # 0.0.0.0:9001:9001/tcp
    ip = "0.0.0.0"
    external = "9001"
    internal = "9001"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  networks_advanced {
    name = "${docker_network.back.name}"
    aliases = [
      "mqtt"
    ]
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/mqtt/mosquitto.conf"
    container_path = "/mosquitto/config/mosquitto.conf"
    read_only = true
  }
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "9001"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "cam1" {
  image = "${docker_image.python.latest}"
  name = "${var.chall_id}-cam1"
  hostname = "${var.chall_id}-cam1"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.cam1.env): "${env}=${lookup(var.cam1.env, env)}"]
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/cam"
    container_path = "/data"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/cam1"
    container_path = "/tmp/img"
    read_only = true
  }
  command = ["python3", "-u", "app.py"]
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "cam2" {
  image = "${docker_image.python.latest}"
  name = "${var.chall_id}-cam2"
  hostname = "${var.chall_id}-cam2"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.cam2.env): "${env}=${lookup(var.cam2.env, env)}"]
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/cam"
    container_path = "/data"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/cam2"
    container_path = "/tmp/img"
    read_only = true
  }
  command = ["python3", "-u", "app.py"]
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

resource "docker_image" "flask" {
  name = "creased/flask:alpine"
}

resource "docker_image" "mqtt" {
  name = "eclipse-mosquitto:latest"
}

resource "docker_image" "python" {
  name = "creased/python:pil"
}
