#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import environ

# environment variables.
APP_HOST = environ.get('APP_HOST', '127.0.0.1')
APP_PORT = int(environ.get('APP_PORT', 8080))
APP_SECRET_KEY = environ.get('APP_SECRET_KEY', '12345')
MQTT_BROKER_HOST = environ.get('MQTT_BROKER_HOST', 'mqtt')
MQTT_BROKER_PORT = int(environ.get('MQTT_BROKER_PORT', 9001))
MQTT_TOPIC = environ.get('MQTT_TOPIC', '/cam1')
