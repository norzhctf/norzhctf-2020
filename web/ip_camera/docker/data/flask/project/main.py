#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template)
from .config import (MQTT_BROKER_HOST, MQTT_BROKER_PORT, MQTT_TOPIC)

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
def index():
    return render_template('index.html', vars={'MQTT_BROKER_HOST': MQTT_BROKER_HOST, 'MQTT_BROKER_PORT': MQTT_BROKER_PORT, 'MQTT_TOPIC': MQTT_TOPIC}), 200
