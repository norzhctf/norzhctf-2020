#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from project import (create_app)
from project.config import (APP_HOST, APP_PORT)

if __name__ == '__main__':
    app = create_app()
    app.run(host=APP_HOST, port=APP_PORT)
