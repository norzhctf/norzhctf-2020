#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os
import time
import base64
import paho.mqtt.client as paho

from datetime import datetime
from PIL import (Image, ImageDraw, ImageFont)

# Configuration.
BASE_IMG_FOLDER = os.environ.get('BASE_IMG_FOLDER', 'img')
BASE_IMG_FILE = os.environ.get('BASE_IMG_FILE', '^base-([0-9]+).jpg$')  # convert -coalesce homer.gif base.jpg
NEW_IMG_FOLDER = os.environ.get('NEW_IMG_FOLDER', '/tmp')
CAMERA_NAME = os.environ.get('CAMERA_NAME', 'cam')
TEXT_FONT = os.environ.get('TEXT_FONT', 'sans-serif.ttf')

MQTT_BROKER_HOST = os.environ.get('MQTT_BROKER_HOST', 'mqtt')
MQTT_BROKER_PORT = int(os.environ.get('MQTT_BROKER_PORT', '1883'), 10)
MQTT_BROKER_TRANSPORT = os.environ.get('MQTT_BROKER_TRANSPORT', 'tcp')
MQTT_TOPIC = os.environ.get('MQTT_TOPIC', '/cam')

REFRESH_TIME = float(os.environ.get('REFRESH_TIME', '0.01'))

TEXT_MARGIN = 15

def base64_image(img_file):
    with open(f'{NEW_IMG_FOLDER}/{img_file}', 'rb') as fd:
        img_b64 = f'data:image/jpeg;base64,{base64.b64encode(fd.read()).decode()}'

    return img_b64

def add_data(img_file, data):
    """Add data to image."""
    pos_helpers = {
        'tl': lambda img_size, text_size: (TEXT_MARGIN, TEXT_MARGIN),
        'tr': lambda img_size, text_size: ((img_size[0] - text_size[0]) - TEXT_MARGIN, TEXT_MARGIN),
        'tc': lambda img_size, text_size: ((img_size[0] - text_size[0]) // 2, TEXT_MARGIN),
        'cl': lambda img_size, text_size: (TEXT_MARGIN, ((img_size[1] - text_size[1]) // 2) - TEXT_MARGIN),
        'cr': lambda img_size, text_size: ((img_size[0] - text_size[0]) - TEXT_MARGIN, ((img_size[1] - text_size[1]) // 2) - TEXT_MARGIN),
        'cc': lambda img_size, text_size: ((img_size[0] - text_size[0]) // 2, ((img_size[1] - text_size[1]) // 2) - TEXT_MARGIN),
        'bl': lambda img_size, text_size: (TEXT_MARGIN, (img_size[1] - text_size[1]) - TEXT_MARGIN),
        'br': lambda img_size, text_size: ((img_size[0] - text_size[0]) - TEXT_MARGIN, (img_size[1] - text_size[1]) - TEXT_MARGIN),
        'bc': lambda img_size, text_size: ((img_size[0] - text_size[0]) // 2, (img_size[1] - text_size[1]) - TEXT_MARGIN)
    }

    img = Image.open(f'{BASE_IMG_FOLDER}/{img_file}')
    draw = ImageDraw.Draw(img)

    prev_rect_lines = list()

    if ('tl' in data or  # top-left
        'tr' in data or  # top-right
        'tc' in data or  # top-center
        'bl' in data or  # bottom-left
        'br' in data or  # bottom-right
        'bc' in data or  # bottom-center
        'cl' in data or  # center-left
        'cr' in data or  # center-right
        'cc' in data):   # center-center

        for position in data:
            text = data[position]

            font = ImageFont.truetype(TEXT_FONT, 16)

            img_size = img.size
            text_size = draw.textsize(text, font)

            text_position = pos_helpers[position](img_size, text_size)

            line = text_position[1] - TEXT_MARGIN
            if line not in prev_rect_lines:
                rectangle_points = ((0, line), (img_size[0], text_position[1] + text_size[1] + TEXT_MARGIN))
                draw.rectangle(rectangle_points, fill='black')
                prev_rect_lines.append(line)

            draw.text(text_position, text, fill='white', font=font)

        img.save(f'{NEW_IMG_FOLDER}/{img_file}')

        return img_file
    else:
        raise ValueError('Specify data with its position!')

if __name__ == '__main__':
    ## MQTT client.
    mqtt_client = paho.Client(CAMERA_NAME, transport=MQTT_BROKER_TRANSPORT)
    mqtt_client.connect(MQTT_BROKER_HOST, MQTT_BROKER_PORT)
    print(f'Connected to {MQTT_BROKER_HOST}:{MQTT_BROKER_PORT} {MQTT_TOPIC}')

    ## Create images every seconds.
    while True:
       for img_file in sorted(filter(re.compile(BASE_IMG_FILE).match,
                              os.listdir(BASE_IMG_FOLDER)),
                              key=lambda image: int(re.search(re.compile(BASE_IMG_FILE), image).groups()[0])):

        data = {
            'tl': datetime.now().strftime('%m/%d/%Y %I:%M:%S%p'),
            'tr': CAMERA_NAME,
        }

        img = add_data(img_file, data)

        img_b64 = base64_image(img_file)

        mqtt_client.publish(MQTT_TOPIC, img_b64)

        time.sleep(REFRESH_TIME)