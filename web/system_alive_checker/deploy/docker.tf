# Create a new variable
variable "network" {
  type = object({
    domain_name = string,
    dns = list(string)
  })
}

variable "chall_event" {
  type = string
  description = "The event where the challenge will be played on."
}

variable "chall_id" {
  type = string
  description = "The id of the challenge to deploy."
}

variable "flask" {
  type = object({
    env = object({
      APP_HOST = string
      APP_PORT = number
      APP_SECRET_KEY = string
      DB_HOST = string
      DB_NAME = string
      DB_USER = string
      DB_PASS = string
    })
  })
}

variable "worker" {
  type = object({
    env = object({
      PYTHONUNBUFFERED = string
      DB_HOST = string
      DB_NAME = string
      DB_USER = string
      DB_PASS = string
      WORKER_THREADS = number
      WORKER_INTERVAL = number
      WORKER_PING_COUNT = number
    })
  })
}

variable "mariadb" {
  type = object({
    env = object({
      MYSQL_DATABASE = string
      MYSQL_PASSWORD = string
      MYSQL_ROOT_PASSWORD = string
      MYSQL_USER = string
    })
  })
}

variable "grafana" {
  type = object({
    env = object({
      GF_SERVER_ROOT_URL = string
      GF_SECURITY_ADMIN_PASSWORD = string
    })
  })
}

variable "chall_host" {
  type = object({
    protocol = string
    username = string
    hostname = string
    port = number
    private_key = string
  })
  default = {
    protocol = "ssh"
    username = "root"
    hostname = ""
    port = 22
    private_key = ".ssh/id_rsa"
  }
  description = "The id of the challenge to deploy."
}

# Copy docker files to the remote server
resource "null_resource" "docker_file_provisionner" {
  connection {
    type = "${var.chall_host.protocol}"
    user = "${var.chall_host.username}"
    host = "${var.chall_host.hostname}"
    port = "${var.chall_host.port}"
    private_key = file("${var.chall_host.private_key}")
    agent = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }

  provisioner "file" {
    source = "../docker/conf"
    destination = "/opt/${var.chall_event}/${var.chall_id}/conf"
  }

  provisioner "file" {
    source = "../docker/data"
    destination = "/opt/${var.chall_event}/${var.chall_id}/data"
  }

  provisioner "file" {
    source = "../docker/log"
    destination = "/opt/${var.chall_event}/${var.chall_id}/log"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir /opt/${var.chall_event}/${var.chall_id}/data/grafana",
      "chmod -R 777 /opt/${var.chall_event}/${var.chall_id}/log /opt/${var.chall_event}/${var.chall_id}/data/grafana"
    ]
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "rm -rf /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }
}

# Configure a docker provider
provider "docker" {
  host = "${var.chall_host.protocol}://${var.chall_host.username}@${var.chall_host.hostname}:${var.chall_host.port}"
}

# Create a new docker network
resource "docker_network" "front" {
  name = "${var.chall_id}-front"
  driver = "bridge"
}

resource "docker_network" "back" {
  name = "${var.chall_id}-back"
  driver = "bridge"
  internal = true
}

# Create a new docker container
resource "docker_container" "flask" {
  image = "${docker_image.flask.latest}"
  name = "${var.chall_id}-flask"
  hostname = "${var.chall_id}-flask"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.flask.env): "${env}=${lookup(var.flask.env, env)}"]
  ports { # 127.0.0.1:8000:8000/tcp
    ip = "127.0.0.1"
    external = "${var.flask.env.APP_PORT}"
    internal = "${var.flask.env.APP_PORT}"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/flask"
    container_path = "/data"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/flask/db"
    container_path = "/data/db"
    read_only = false
  }
  command = ["python3", "app.py"]
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "${var.flask.env.APP_PORT}"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "worker" {
  image = "${docker_image.python.latest}"
  name = "${var.chall_id}-worker"
  hostname = "${var.chall_id}-worker"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.worker.env): "${env}=${lookup(var.worker.env, env)}"]
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/worker"
    container_path = "/data"
    read_only = true
  }
  command = ["python3", "app.py"]
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "db" {
  image = "${docker_image.mariadb.latest}"
  name = "${var.chall_id}-db"
  hostname = "${var.chall_id}-db"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.mariadb.env): "${env}=${lookup(var.mariadb.env, env)}"]
  networks_advanced {
    name = "${docker_network.back.name}"
    aliases = [
      "db"
    ]
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/mariadb/my.cnf"
    container_path = "/etc/mysql/my.cnf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/mariadb"
    container_path = "/var/lib/mysql"
    read_only = false
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/mariadb/database.sql"
    container_path = "/docker-entrypoint-initdb.d/database.sql"
    read_only = true
  }
  healthcheck {
    test = ["CMD", "mysqladmin", "-u${var.mariadb.env.MYSQL_USER}", "-p${var.mariadb.env.MYSQL_PASSWORD}", "ping", "-h", "localhost"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "http" {
  image = "${docker_image.proxy.latest}"
  name = "${var.chall_id}-proxy"
  hostname = "${var.chall_id}-proxy"
  domainname = "${var.network.domain_name}"
  network_mode = "host"
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/nginx.conf"
    container_path = "/etc/nginx/nginx.conf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/conf.d"
    container_path = "/etc/nginx/conf.d"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/certs"
    container_path = "/etc/ssl/private"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/log/nginx"
    container_path = "/usr/share/nginx/log"
    read_only = false
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "loki" {
  image = "${docker_image.loki.latest}"
  name = "${var.chall_id}-loki"
  hostname = "${var.chall_id}-loki"
  domainname = "${var.network.domain_name}"
  networks_advanced {
    name = "${docker_network.back.name}"
    aliases = [
      "loki"
    ]
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/loki/local-config.yaml"
    container_path = "/etc/loki/local-config.yaml"
    read_only = true
  }
  command = ["-config.file=/etc/loki/local-config.yaml"]
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "3100"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "promtail" {
  image = "${docker_image.promtail.latest}"
  name = "${var.chall_id}-promtail"
  hostname = "${var.chall_id}-promtail"
  domainname = "${var.network.domain_name}"
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/log/nginx"
    container_path = "/var/log"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/promtail/docker-config.yaml"
    container_path = "/etc/promtail/docker-config.yaml"
    read_only = true
  }
  depends_on = [docker_container.loki]
  command = ["-config.file=/etc/promtail/docker-config.yaml"]
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "grafana" {
  image = "${docker_image.grafana.latest}"
  name = "${var.chall_id}-grafana"
  hostname = "${var.chall_id}-grafana"
  domainname = "${var.network.domain_name}"
  env = [for env in keys(var.grafana.env): "${env}=${lookup(var.grafana.env, env)}"]
  ports { # 127.0.0.1:3000:3000/tcp
    ip = "127.0.0.1"
    external = "3000"
    internal = "3000"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/grafana"
    container_path = "/etc/grafana/provisioning"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/grafana"
    container_path = "/var/lib/grafana"
    read_only = false
  }
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "3000"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

resource "docker_image" "flask" {
  name = "creased/flask:alpine-pymysql"
}

resource "docker_image" "python" {
  name = "creased/python:pymysql"
}

resource "docker_image" "mariadb" {
  name = "mariadb:latest"
}

resource "docker_image" "proxy" {
  name = "creased/webdev-env-http:latest"
}

resource "docker_image" "promtail" {
  name = "grafana/promtail:latest"
}

resource "docker_image" "loki" {
  name = "grafana/loki:latest"
}

resource "docker_image" "grafana" {
  name = "grafana/grafana:latest"
}
