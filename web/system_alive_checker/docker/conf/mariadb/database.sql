SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE db;

DROP TABLE IF EXISTS `hosts`;
CREATE TABLE `hosts` (
  `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL UNIQUE,
  `ip` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `results`;
CREATE TABLE `results` (
  `host` INT NOT NULL,
  `date` TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `time` FLOAT(10,4) NOT NULL,
  FOREIGN KEY(host) REFERENCES hosts(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `hosts` (`name`, `ip`) VALUES ('PLC1', '10.13.67.33'),
                                          ('PLC2', '10.13.67.34'),
                                          ('not a real host', 'ENSIBS{D4taBaSE_Is0l4t1oN}');
