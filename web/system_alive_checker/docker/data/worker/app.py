#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Original: https://raw.githubusercontent.com/mahirrudin/pyng-sql/master/ipv4.ping.py

from threading import Thread
import os
import re
import time
import traceback
import subprocess
import pymysql.cursors

try:
   import queue as Queue
except ImportError:
   import Queue

# Configuration.
DB_HOST = os.environ.get('DB_HOST', 'db')
DB_NAME = os.environ.get('DB_NAME', 'app')
DB_USER = os.environ.get('DB_USER', 'admin-app')
DB_PASS = os.environ.get('DB_PASS', 'app-admin')

THREADS = int(os.environ.get('WORKER_THREADS', 4))
INTERVAL = int(os.environ.get('WORKER_INTERVAL', 3))
PING_COUNT = int(os.environ.get('WORKER_PING_COUNT', 4))

# Global variables.
hosts_q = Queue.Queue()
results_q = Queue.Queue()
ping_threads = list()
write_threads = list()

def get_connection():
    """Open mysql connection."""
    try:
        connection = pymysql.connect(host = DB_HOST,
                                    db = DB_NAME,
                                    user = DB_USER,
                                    password = DB_PASS,
                                    cursorclass = pymysql.cursors.DictCursor)
    except:
        raise ConnectionRefusedError()
    else:
        return connection

def ping_thd(thread_id, queue):
    """Pings hosts in the queue."""
    while True:
        try:
            # get an host from the queue.
            host = queue.get()

            # start ping process.
            ping_process = subprocess.Popen(f'/bin/ping -c{PING_COUNT} -W1 {host["ip"]}', shell=True, stdout=subprocess.PIPE)

            # save ping result.
            result = ping_process.communicate()[0].decode()

            # parse ping result.
            ping_data = re.search(r'rtt min\/avg\/max\/mdev = (?P<min>[0-9]+.[0-9]{3,})\/(?P<avg>[0-9]+.[0-9]{3,})\/(?P<max>[0-9]+.[0-9]{3,})\/(?P<mdev>[0-9]+.[0-9]{3,}) ms', result, re.M)
            if not ping_data:  # handles busybox ping result.
                ping_data = re.search(r'round-trip min\/avg\/max = (?P<min>[0-9]+.[0-9]{3,})\/(?P<avg>[0-9]+.[0-9]{3,})\/(?P<max>[0-9]+.[0-9]{3,}) ms', result, re.M)

            if ping_data:
                print(f'Debug from thread Ping-{thread_id}: {host["name"]} - {ping_data.group("avg")}')

                # add result to the queue.
                results_q.put({'host': host['name'], 'time': ping_data.group('avg')})
            else:
                raise ValueError(f'Something goes wrong with IP: {host["ip"]}...')
        except ValueError as err:
            print(f'Value error in thread Ping-{thread_id}: {err}')
        except:
            print(f'Exception in thread Ping-{thread_id}:')
            traceback.print_exc()
        finally:
            # IP has been processed.
            queue.task_done()

def write_db_thd(thread_id, queue):
    """Write ping results to the database."""
    while True:
        try:
            # get a ping result from the queue.
            result = queue.get()

            print(f'Debug from thread Write-{thread_id}: {result["host"]} - {result["time"]}')

            # insert the result to the database.
            connection = get_connection()
            with connection.cursor() as cursor:
                sql = 'INSERT INTO results (host, time) VALUES ((SELECT id FROM hosts WHERE name = %s), %s);'
                cursor.execute(sql, (result['host'], result['time'],))
                connection.commit()
        except:
            print(f'Exception in thread Write-{thread_id}:')
            traceback.print_exc()
        finally:
            # result has been processed.
            connection.close()
            queue.task_done()

if __name__ == '__main__':
    ## Start threads.
    while True:
        try:
            # Spawn threads.
            if len(ping_threads) < (THREADS // 2):
                for i in range(len(ping_threads), (THREADS // 2) - len(ping_threads)):
                    worker = Thread(target=ping_thd, args=(i, hosts_q), name=f'Ping-{i}')
                    worker.setDaemon(True)
                    worker.start()

            if len(write_threads) < (THREADS // 2):
                for i in range(len(write_threads), (THREADS // 2) - len(write_threads)):
                    worker = Thread(target=write_db_thd, args=(i, results_q), name=f'Write-{i}')
                    worker.setDaemon(True)
                    worker.start()

            # get IP list.
            connection = get_connection()
            with connection.cursor() as cursor:
                sql = "SELECT name, ip FROM hosts;"
                cursor.execute(sql)
                hosts = cursor.fetchall()
            connection.close()

            for host in hosts:
                hosts_q.put({'name': host['name'], 'ip': host['ip']})
                hosts_q.join()

            time.sleep(INTERVAL)
        except ConnectionRefusedError:
            print('An error occurred while trying to connect to database, retrying in 5 seconds...')
            time.sleep(5)
        except:
            traceback.print_exc()