#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from project import (create_app, db)
from project.config import (APP_HOST, APP_PORT)
from werkzeug.contrib.fixers import ProxyFix

if __name__ == '__main__':
    app = create_app()
    db.create_all(app=app)
    app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_host=1)
    app.run(host=APP_HOST, port=APP_PORT)
