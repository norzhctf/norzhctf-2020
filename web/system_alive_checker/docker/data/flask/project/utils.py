#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def in_dictlist(key, value, dictlist):
    """Search for a key and value in a dict list and return the index."""
    for i in range(len(dictlist)):
        if dictlist[i][key] == value:
            return i
    return -1
