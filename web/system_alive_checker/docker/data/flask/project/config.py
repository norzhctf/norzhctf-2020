#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import environ

# environment variables.
APP_HOST = environ.get('APP_HOST', '127.0.0.1')
APP_PORT = int(environ.get('APP_PORT', 8080))
APP_SECRET_KEY = environ.get('APP_SECRET_KEY', '12345')
APP_SQLITE_DB = environ.get('APP_SQLITE_DB', 'sqlite:////data/db/user.db')

DB_HOST = environ.get('DB_HOST', 'db')
DB_NAME = environ.get('DB_NAME', 'app')
DB_USER = environ.get('DB_USER', 'admin-app')
DB_PASS = environ.get('DB_PASS', 'app-admin')
