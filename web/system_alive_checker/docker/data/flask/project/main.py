#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, jsonify, request)
from flask_login import (login_required, current_user)
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from .utils import in_dictlist
from . import (db, get_mysql_connection)

main = Blueprint('main', __name__)

@main.route('/', methods=['GET'])
def index():
    return render_template('index.html', page='Home'), 200

@main.route('/dashboard', methods=['GET'])
@login_required
@jwt_required
def dashboard():
    identity = get_jwt_identity()
    if 'role' in identity and identity['role'] == 'admin':
        if current_user.username == 'admin':
            # flag = 'ENSIBS{B3w4re_oF_y0uR_L0gG1nG_ruL3S!}'
            flag = 'A flag is waiting for you... Find a way to impersonate an admin account ;)'
        else:
            flag = 'Congrats, here is your flag: ENSIBS{JS0N_W€B_ToKen_iS_Cr4p...}'
        response = render_template('dashboard.html', page='Dashboard', flag=flag), 200
    else:
        response = render_template('error.html', page='Access Denied', message='A user with admin role is required to access this resource!'), 403

    return response

@main.route('/dashboard', methods=['POST'])
@login_required
@jwt_required
def dashboard_post():
    identity = get_jwt_identity()
    if 'role' in identity and identity['role'] == 'admin':
        hostname = request.form['hostname']
        ip = request.form['ip']

        connection = get_mysql_connection()
        with connection.cursor() as cursor:
            sql = 'INSERT INTO hosts (name, ip) VALUES (%s, %s);'
            cursor.execute(sql, (hostname, ip,))
            connection.commit()
        connection.close()

        response = f'{hostname} has been added to the database.', 200
    else:
        response = render_template('error.html', page='Access Denied', message='A user with admin role is required to access this resource!'), 403

    return response

@main.route('/stats', methods=['GET'])
@login_required
@jwt_required
def stats():
    identity = get_jwt_identity()
    if 'role' in identity and identity['role'] == 'admin':
        connection = get_mysql_connection()
        with connection.cursor() as cursor:
            sql = 'SELECT hosts.name, ROUND(UNIX_TIMESTAMP(date) * 1000) as date, time FROM results LEFT JOIN hosts ON hosts.id = results.host ORDER BY hosts.name, date ASC;'
            cursor.execute(sql)
            results = cursor.fetchall()
        connection.close()

        results_list = list()
        for result in results:
            index = in_dictlist('label', result['name'], results_list)
            if index == -1:
                results_list.append({'data': list(), 'label': result['name']})
            results_list[index]['data'].append([int(result['date']), float(result['time'])])

        response = jsonify(results_list), 200
    else:
        response = jsonify({'Access Denied'}), 403

    return response

@main.route('/stats', methods=['DELETE'])
@login_required
@jwt_required
def stats_delete_all():
    identity = get_jwt_identity()
    if 'role' in identity and identity['role'] == 'admin':
        connection = get_mysql_connection()
        with connection.cursor() as cursor:
            sql = 'DELETE FROM hosts;'
            cursor.execute(sql)
            connection.commit()
        connection.close()

        response = f'Stats have been flushed.', 200
    else:
        response = 'Access Denied', 403

    return response

@main.route('/stats/<string:hostname>', methods=['DELETE'])
@login_required
@jwt_required
def stats_delete(hostname):
    identity = get_jwt_identity()
    if 'role' in identity and identity['role'] == 'admin':
        connection = get_mysql_connection()
        with connection.cursor() as cursor:
            sql = 'DELETE FROM hosts WHERE hosts.name = %s;'
            cursor.execute(sql, (hostname,))
            connection.commit()
        connection.close()

        response = f'{hostname} has been removed from the database.', 200
    else:
        response = 'Access Denied', 403

    return response
