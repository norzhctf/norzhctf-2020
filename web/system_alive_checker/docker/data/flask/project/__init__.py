#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import pymysql.cursors

from .config import (APP_SECRET_KEY, APP_SQLITE_DB, DB_HOST, DB_NAME, DB_USER, DB_PASS)
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_jwt_extended import JWTManager

# init SQLAlchemy so we can use it later in our models.
db = SQLAlchemy()

def get_mysql_connection():
    """Open mysql connection."""
    connection = pymysql.connect(host = DB_HOST,
                                db = DB_NAME,
                                user = DB_USER,
                                password = DB_PASS,
                                cursorclass = pymysql.cursors.DictCursor)
    return connection

def create_app():
    app = Flask(__name__)

    config = {
        'DEBUG': False,
        'SECRET_KEY': APP_SECRET_KEY,
        'JWT_SECRET_KEY': APP_SECRET_KEY,
        'JWT_ALGORITHM': 'HS256',
        'JWT_ACCESS_COOKIE_NAME': 'access_token',
        'JWT_SESSION_COOKIE': True,
        'JWT_TOKEN_LOCATION': 'cookies',
        'JWT_ACCESS_COOKIE_PATH': '/',
        'JWT_COOKIE_CSRF_PROTECT': False,
        'JWT_ACCESS_TOKEN_EXPIRES': datetime.timedelta(days=365),
        'JWT_BLACKLIST_ENABLED': False,
        'SQLALCHEMY_DATABASE_URI': APP_SQLITE_DB,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    }

    app.config.from_mapping(config)
    app.url_map.strict_slashes = False

    db.init_app(app)

    jwt = JWTManager(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
