#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import (Blueprint, render_template, redirect, url_for, request, flash)
from flask_login import (login_user, logout_user, login_required)
from flask_jwt_extended import (create_access_token, set_access_cookies, unset_jwt_cookies)
from werkzeug.security import (generate_password_hash, check_password_hash)
from .models import User
from . import db

auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['GET'])
def login():
    return render_template('login.html', page='Login'), 200

@auth.route('/signup', methods=['GET'])
def signup():
    return render_template('signup.html', page='Sign Up'), 200

@auth.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    response = redirect(url_for('main.index'))
    unset_jwt_cookies(response)
    return response

@auth.route('/login', methods=['POST'])
def login_post():
    username = request.form.get('username')
    password = request.form.get('password')

    user = User.query.filter_by(username=username).first()

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not check_password_hash(user.password, password): 
        flash('Please check your login details and try again.')
        response = redirect(url_for('auth.login')) # if user doesn't exist or password is wrong, reload the page
    else:
        login_user(user)

        access_token = create_access_token(identity = {'role': user.role})
        response = redirect(url_for('main.index'))
        set_access_cookies(response, access_token)

    return response

@auth.route('/signup', methods=['POST'])
def signup_post():
    username = request.form.get('username')
    password = request.form.get('password')

    user = User.query.filter_by(username=username).first() # if this returns a user, then the user already exists in database

    if user: # if a user is found, we want to redirect back to signup page so user can try again
        flash('User already exists')
        response = redirect(url_for('auth.signup'))
    else:
        # create new user with the form data. Hash the password so plaintext version isn't saved.
        new_user = User(username=username, password=generate_password_hash(password, method='sha256'), role='user')

        # add the new user to the database
        db.session.add(new_user)
        db.session.commit()

        response = redirect(url_for('auth.login'))
    
    return response
