# Extranet Norzh Nuclea

Vous avez trouvé l'extranet de l'entreprise.

Ce challenge se décompose en 4 parties :
- Obtenir un accés à l'interface administrateur
- Obtenir un shell sur la machine
- Obtenir un accès root sur la machine
- Obtenir le mot de passe de l'utilisateur `web-dev`

## Chall resources

## Hints

None