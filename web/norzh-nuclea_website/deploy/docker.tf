# Create a new variable
variable "network" {
  type = object({
    domain_name = string,
    dns = list(string)
  })
}

variable "chall_event" {
  type = string
  description = "The event where the challenge will be played on."
}

variable "chall_id" {
  type = string
  description = "The id of the challenge to deploy."
}

variable "chall_host" {
  type = object({
    protocol = string
    username = string
    hostname = string
    port = number
    private_key = string
  })
  default = {
    protocol = "ssh"
    username = "root"
    hostname = ""
    port = 22
    private_key = ".ssh/id_rsa"
  }
  description = "The id of the challenge to deploy."
}

# Copy docker files to the remote server
resource "null_resource" "docker_file_provisionner" {
  connection {
    type = "${var.chall_host.protocol}"
    user = "${var.chall_host.username}"
    host = "${var.chall_host.hostname}"
    port = "${var.chall_host.port}"
    private_key = file("${var.chall_host.private_key}")
    agent = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }

  provisioner "file" {
    source = "../docker/conf"
    destination = "/opt/${var.chall_event}/${var.chall_id}/conf"
  }

  provisioner "file" {
    source = "../docker/data"
    destination = "/opt/${var.chall_event}/${var.chall_id}/data"
  }

  provisioner "file" {
    source = "../docker/log"
    destination = "/opt/${var.chall_event}/${var.chall_id}/log"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /opt/${var.chall_event}/${var.chall_id}/log/nginx",
      "chmod -R 777 /opt/${var.chall_event}/${var.chall_id}/log /opt/${var.chall_event}/${var.chall_id}/data/mongo",
      "chown -R 999:999 /opt/${var.chall_event}/${var.chall_id}/data/mongo"
    ]
  }

  provisioner "remote-exec" {
    when = "destroy"
    inline = [
      "rm -rf /opt/${var.chall_event}/${var.chall_id}/"
    ]
  }
}

# Configure a docker provider
provider "docker" {
  host = "${var.chall_host.protocol}://${var.chall_host.username}@${var.chall_host.hostname}:${var.chall_host.port}"
}

# Create a new docker network
resource "docker_network" "front" {
  name = "${var.chall_id}-front"
  driver = "bridge"
}

resource "docker_network" "back" {
  name = "${var.chall_id}-back"
  driver = "bridge"
  internal = true
}

# Create a new docker container
resource "docker_container" "node" {
  image = "${docker_image.node.latest}"
  name = "${var.chall_id}-node"
  hostname = "${var.chall_id}-node"
  domainname = "${var.network.domain_name}"
  ports { # 127.0.0.1:8080:8080/tcp
    ip = "127.0.0.1"
    external = "8080"
    internal = "8080"
    protocol = "tcp"
  }
  ports { # 0.0.0.0:3000:3000/tcp
    ip = "0.0.0.0"
    external = "3000"
    internal = "3000"
    protocol = "tcp"
  }
  networks_advanced {
    name = "${docker_network.front.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  networks_advanced {
    name = "${docker_network.back.name}"
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/node/root_flag"
    container_path = "/root/flag"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/node/user_flag"
    container_path = "/home/web-dev/flag"
    read_only = true
  }
  healthcheck {
    test = ["CMD", "nc", "-z", "localhost", "8080"]
    interval = "10s"
    timeout = "10s"
    retries = 3
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "http" {
  image = "${docker_image.proxy.latest}"
  name = "${var.chall_id}-proxy"
  hostname = "${var.chall_id}-proxy"
  domainname = "${var.network.domain_name}"
  network_mode = "host"
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/nginx.conf"
    container_path = "/etc/nginx/nginx.conf"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/conf/nginx/conf.d"
    container_path = "/etc/nginx/conf.d"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/certs"
    container_path = "/etc/ssl/private"
    read_only = true
  }
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/log/nginx"
    container_path = "/usr/share/nginx/log"
    read_only = false
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

# Create a new docker container
resource "docker_container" "mongo" {
  image = "${docker_image.mongo.latest}"
  name = "${var.chall_id}-mongo"
  hostname = "${var.chall_id}-mongo"
  domainname = "${var.network.domain_name}"
  networks_advanced {
    name = "${docker_network.back.name}"
    aliases = [
      "mongo"
    ]
    # ipv4_address = "10.0.0.1"
    # ipv6_address = ""
  }
  dns = "${var.network.dns}"
  restart = "always"
  volumes {
    host_path = "/opt/${var.chall_event}/${var.chall_id}/data/mongo"
    container_path = "/data/db"
    read_only = false
  }
  rm = false
  start = true
  must_run = true
  privileged = false
  memory = 2048
  memory_swap = -1
}

resource "docker_image" "mongo" {
  name = "mongo:latest"
}

resource "docker_image" "node" {
  name = "creased/node:express"
}

resource "docker_image" "proxy" {
  name = "creased/webdev-env-http:latest"
}