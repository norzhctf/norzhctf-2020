#!/bin/bash

while [ "$(curl -s -o /dev/null -w '%{http_code}' http://mongo:27017)" != "200" ]; do sleep 5; done

# Script used to start the two nodemon servers one is for healthcheck, the other is for the web server
# Launched as root
sudo /usr/local/bin/nodemon /healthcheck/server.js &

# Launched as current user (for security )
/usr/local/bin/nodemon /webserver/graphql-api/index.js &
/usr/local/bin/nodemon /webserver/blog/index.js