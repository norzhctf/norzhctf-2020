var request = require('sync-request');

const requestGraphQl = (url, hex = false) => {
    if(hex)
        url = 'http://localhost:4000' + Buffer.from(url,'hex').toString('ascii')
    console.log(url)
    try {
        var res = request('GET', url);
        return res.getBody()
    } catch (e) {
        console.log(e)
        return ""
    }
}

const getArticles = () => {
    return JSON.parse(requestGraphQl("http://localhost:4000/graphql/?query=query{%0Ablogposts{%0A_id%0Atitle%0Acontent%0A}%0A}",false))
}

const validateCredentials = (username,password) =>
{
    console.log(username,password)
    var users = JSON.parse(requestGraphQl("http://localhost:4000/graphql?query=query%20%7B%0A%20%20users%7B%0A%20%20%20%20username%0A%20%20%20%20password%0A%20%20%7D%0A%7D",false)).data.users
    users = users.filter((user) => {return user.username == username && user.password == password})
    return users.length > 0
}

const evalBrackets = (content) => {
    const regex = /\{.*?\}/gm;
    let m;
    var str = content

    console.log("Before :" + str)
    while ((m = regex.exec(content)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }
        
        for(var i=0; i<m.length;i++){
            match = m[i]
            console.log(`Found match, group ${i}: ${match}`);
            str = str.replace(match, eval(match.replace("{","").replace("}","")))
        }        
    }
    console.log("After : " + str)
    return str

}
const addArticle = (title, content) => {
    var evaluated_content = evalBrackets(content)
    url = "http://localhost:4000/graphql"
    try {
        var res = request('POST', url,{
            json: {
                operationName: "createPost",
                query:"mutation createPost($title:String!,$content:String!){createBlogPost(title:$title,content:$content){title,content}}",
                variables: {
                    title: title,
                    content: evaluated_content
                }
            }
        });
        return true
    } catch (e) {
        console.log(e)
        return false
    }
}

module.exports = {
    requestGraphQl: requestGraphQl,
    validateCredentials : validateCredentials,
    getArticles : getArticles,
    addArticle : addArticle
}