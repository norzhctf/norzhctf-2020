const blog_list_views = (json_blog) =>{
    var bloglist = ''
    console.log(json_blog)

   

    for(var i=0; i<json_blog.length; i++){
        var url = '/blog/'+Buffer.from(`/graphql?query=query {blogpost(_id:"${json_blog[i]._id}"){_id title content}}`, 'utf8').toString('hex')
        bloglist+=`
            <div class="card mb-4">
            <div class="card-body">
                <h2 class="card-title"><a href="#" style="color: inherit">${json_blog[i].title}</a></h2>
                <p class="card-text"> ${json_blog[i].content.substring(0, 10) + '...'} </p>
                <a href="${url}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            </div>
        `
    }



    return `
    <!DOCTYPE html>
    <html lang="en">

    <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog - NorzhNuclea</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/blog-home.css" rel="stylesheet">

    </head>

    <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
        <a class="navbar-brand" href="/">NorzhNuclea</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Home
                </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/blog">Blog 
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/login">Login</a>
        </li>
            </ul>
        </div>
        </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4">NorzhNuclea
            <small>Blog Posts</small>
            </h1>

            <!-- Blog Post -->
            
            ${bloglist}


            <!-- Pagination -->

        </div>

        <!-- Sidebar Widgets Column -->
        </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
        <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; NorzhNuclea 2020</p>
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </body>

    </html>
    `
}

const blog_article = (data) => {
    return `<!DOCTYPE html>
    <html lang="en">
    
    <head>
    
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
    
      <title>Blog Post - NorzhNuclea</title>
    
      <!-- Bootstrap core CSS -->
      <link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
      <!-- Custom styles for this template -->
      <link href="/css/blog-post.css" rel="stylesheet">
    
    </head>
    
    <body>
    
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
          <a class="navbar-brand" href="/">NorzhNuclea</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="/">Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/blog">Blog</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/login">Login</a>
           </li>
            </ul>
          </div>
        </div>
      </nav>
    
      <!-- Page Content -->
      <div class="container">
    
        <div class="row">
    
          <!-- Post Content Column -->
          <div class="col-lg-8">
    
            <!-- Title -->
            <h1 class="mt-4">Post Title</h1>
    
            <hr>
    
            <div id="content">
            </div>
            <hr>
    
    
          </div>
    
          <!-- Sidebar Widgets Column -->
          <div class="col-md-4">
    
            <!-- Search Widget -->
            
    
          </div>
    
        </div>
        <!-- /.row -->
    
      </div>
      <!-- /.container -->
    
      <!-- Footer -->
      <footer class="py-5 bg-dark">
        <div class="container">
          <p class="m-0 text-center text-white">Copyright &copy; NorzhNuclea 2020</p>
        </div>
        <!-- /.container -->
      </footer>
    
      <!-- Bootstrap core JavaScript -->
      <script src="/vendor/jquery/jquery.min.js"></script>
      <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script>
        var blogpost = ${JSON.stringify(data)}
        $('document').ready(function(){
            $('.mt-4').text(blogpost.blogpost.title)
            $('#content').html(blogpost.blogpost.content)
        })
      </script>
    
    </body>
    
    </html>`
}

module.exports = {
    blog_list_views:blog_list_views,
    blog_article:blog_article
}