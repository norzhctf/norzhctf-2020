#!/usr/bin/node
const express = require('express')
const session = require('express-session')
const app = express()
const BlogUtil = require('./src/BlogUtil.js')
const Views = require('./src/Views.js')

app.use(session({
    cookieName: 'session',
    secret: 'WoWoWoWSuchSecretCouldHaveBeenAFlagButNotInThisCase:TheGame<3',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000,
}));

app.use(express.json())
app.use(express.static('public'));


app.get('/', (req,res) => {
    return res.sendfile(__dirname + '/templates/index.html')
})

app.get('/blog', (req,res) => {
    var articles = BlogUtil.getArticles().data.blogposts
    return res.send(Views.blog_list_views(articles))
})

app.get('/blog/:query', (req,res) => {
    var body = JSON.parse(BlogUtil.requestGraphQl(req.params.query,true)).data
    return res.send(Views.blog_article(body))
})

app.get('/login',(req,res)=>{
    return res.sendFile(__dirname + '/templates/login.html');
})

app.post('/login',(req,res)=>{
    if(BlogUtil.validateCredentials(req.body.username,req.body.password)){
        req.session.authenticated = true
        return res.send(true)
    } else {
        req.session.authenticated = false
        return res.send(false)
    }
})

app.get('/admin',(req,res) => {
    if(req.session.authenticated === true){
        return res.sendFile(__dirname+'/templates/blog-input.html')
    } else {
        return res.send('You must be authenticated to access the admin panel !')
    }
})

app.post('/admin', (req,res) => {
    if(req.session.authenticated === true){
        console.log("Adding blog article ", req.body.title, req.body.content)
        var result = BlogUtil.addArticle(req.body.title, req.body.content)
        return res.send(result)
    } else {
        return res.send(false)
    }
})

app.listen(8080, function(){
    console.log("Web-Server launched on port 8080")
})