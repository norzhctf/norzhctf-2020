const {MongoClient, ObjectId} = require( 'mongodb')
const express = require( 'express')
const bodyParser = require( 'body-parser')
const {graphqlExpress, graphiqlExpress} = require( 'graphql-server-express')
const {makeExecutableSchema} = require( 'graphql-tools')
const cors = require( 'cors')



const app = express()

app.use(cors())

const homePath = '/graphiql'
const URL = 'http://localhost'
const PORT = 4000
const MONGO_URL = 'mongodb://mongo:27017/blog'

const prepare = (o) => {
    o._id = o._id.toString()
    return o
}

const start = async () => {
  try {
    const db = await MongoClient.connect(MONGO_URL)

    const BlogPosts = db.collection('BlogPost')
    const Users = db.collection('User')

    const typeDefs = [`
      type Query {
        user(_id: String): User
        users: [User]
        blogpost(_id: String): BlogPost
        blogposts : [BlogPost]
      }

      type BlogPost {
        _id: String
        title: String
        content: String
      }
      
      type User {
        _id: String
        username: String
        password: String
      }

      type Mutation {
        createBlogPost(title: String, content: String): BlogPost
      }
      schema {
        query: Query
        mutation: Mutation
      }
    `];

    const resolvers = {
      Query: {
        user: async (root, {_id}) => {
          return prepare(await Users.findOne(ObjectId(_id)))
        },
        users: async () => {
          return (await Users.find({}).toArray()).map(prepare)
        },
        blogpost: async (root, {_id}) => {
          return prepare(await BlogPosts.findOne(ObjectId(_id)))
        },
        blogposts:  async () => {
            return (await BlogPosts.find({}).toArray()).map(prepare)
          },
      },
      

      Mutation: {
        createBlogPost: async (root, args, context, info) => {
          const res = await BlogPosts.insertOne(args)
          return prepare(res.ops[0])
        }
      },
    }

    const schema = makeExecutableSchema({
      typeDefs,
      resolvers
    })


    app.use('/graphql', bodyParser.json(), graphqlExpress({schema}))


    app.use(homePath, graphiqlExpress({
      endpointURL: '/graphql'
    }))

    app.listen(PORT, () => {
      console.log(`Visit ${URL}:${PORT}${homePath}`)
    })

  } catch (e) {
    console.log(e)
  }

}

start()