#!/usr/bin/node
const express = require('express')
const  express_basic_auth = require('express-basic-auth')
const { spawn } = require('child_process')

const config = {
    PORT : 3000
}

const check_credentials = (user,password) => {
    const process = spawn('/bin/bash',['/healthcheck/authent.sh', user, password ])
    console.log(String(process.stdout))
    return String(process.stdout) == 'True'
}

const basic_auth = express_basic_auth({
    authorizer : check_credentials,
    unauthorizedResponse: (req) => {
        return req.auth
        ? ('Credentials ' + req.auth.user + ':' + "**********" + ' rejected')
        : 'No credentials provided'
    }
})

const app = express()
app.use(basic_auth)


app.get('/ping', (req, res) => {
    return res.send("up")
})

app.get('/health', (req, res) => {
    return res.send(String(spawn('/bin/ps',['-auxfw']).stdout))
})

app.get('/disk-usage', (req, res) => {
    return res.send(String(spawn('/bin/df',['-h']).stdout))
})

app.get('/web-logs', (req, res) => {
    return res.send(String(spawn('/bin/cat',['/var/log/httpd/error_log']).stdout))
})

app.get('/active-connections', (req,res) => {
    return res.send(String(spawn('/bin/netstat',['-laputen']).stdout))
})

app.listen(config.PORT, () =>
  console.log(`Healthcheck app listening on port ${config.PORT}!`),
);
