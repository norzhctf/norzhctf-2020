#!/bin/sh

PATH=/bin:/usr/bin

username=$1
PASSWORD=$2

IFS=''

shadow_line=$(cat /etc/shadow | grep $1)
python3 -c "import crypt;salt='$shadow_line'.split('\$6\$')[1].split('$')[0];hash = crypt.crypt('$PASSWORD', '\$6\$'+salt+'$');print(hash == '$shadow_line'.split(':')[1],end='')"
