#!/bin/bash
mongoimport --host mongo --db blog --collection User --type json --drop --file /users.json --jsonArray &
mongoimport --host mongo --db blog --collection BlogPost --type json --drop --file /blogposts.json --jsonArray